#ifndef PROJECTMANAGER_H
#define PROJECTMANAGER_H

#include <QObject>
#include <list>
#include "project.h"
#include "painter_contexts/painter.h"
#include "projectdialog.h"

namespace Ui {
class MainWindow;
}
class MainWindow;

typedef cv::Mat CovMatrix;
typedef std::map<QString, Project*> ProjectsMap;

class ProjectManager : public QObject {
    Q_OBJECT
public:
    explicit ProjectManager(MainWindow* parent = 0);
    ~ProjectManager();
    Project* operator []( QString & name ) { return projects_[name]; }

private:
    MainWindow * main_window_;
    QString current_project_name_;
    ProjectsMap projects_;

public slots:
    void createProject( QString& name, QString& file_name );
    void createProject();
    void deleteProject( QString& name );

    void readProject( const QString& file_name );
    void readProject();
    void closeProject();

    void        setCurrentProject( QString & name ) { current_project_name_ = name; }
    Project*    getCurrentProject() { return projects_[current_project_name_]; }

    void readConfig();
    void writeConfig();
};

#endif // PROJECTMANAGER_H
