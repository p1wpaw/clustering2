#ifndef DISTANCEFUNCTONS_H
#define DISTANCEFUNCTONS_H

#include <stdlib.h>
#include <opencv2/core/core.hpp>
#include "cluster.h"

typedef cv::Mat CovMatrix;

class DistanceFuncton {
    // *** for L_P_Norm distance function
private:
    static int l_, p_;
public:
    static void setLP( int l=1, int p=1 );
    static std::pair<int, int> getLP();
    // /***

public:
    static double euclidean( Data& x, Data& y, CovMatrix * cov_matr = 0 );
    static double l_p_norm( Data& x, Data& y, CovMatrix * cov_matr = 0 );
    static double ward( Data& x, Data& y, CovMatrix * cov_matr = 0 );

};


//typedef double (DistanceFuncton::* DistanceFunc)(Data&, Data&, CovMatrix *);  //  -- ������������ ����������� ���� ��������� �� ����������� �����
typedef double (*DistanceFunc)(Data&, Data&, CovMatrix *); //  -- ���������� ����������� ���� ��������� �� ����������� �����
typedef std::pair<DistanceFunc, QString> NamedDistanceFunc;
#endif // DISTANCEFUNCTONS_H
