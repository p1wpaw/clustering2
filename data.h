#ifndef DATA_H
#define DATA_H

#include <qglobal.h>
#include <QRgb>
#include <QPoint>
#include <vector>
#include <set>
#include <opencv2/core/core.hpp>
#include <QMutex>

class Data { // map opencv image to vector
public:
    typedef struct { int i, j; } DataIndex;
    class OffsetOperator {
    protected:
        Data::DataIndex     offset_;
        cv::Mat*            labels_;

    public:
        OffsetOperator() {}
        OffsetOperator( cv::Mat* labels,
                        const QPoint& offset = QPoint( 0, 0 ) );

        OffsetOperator( cv::Mat* labels,
                        const Data::DataIndex& offset = Data::DataIndex({ 0, 0 }) );

        // two below methods returns index+offset value
        Data::DataIndex offsetFromIndex( int i, int j );
        Data::DataIndex offsetFromIndex( const Data::DataIndex& index );

        // two below ethods returns normal index of offset value
        Data::DataIndex indexFromOffset( int i, int j );
        Data::DataIndex indexFromOffset( const Data::DataIndex& index );

        Data::DataIndex getOffset() { return offset_; }
        QPoint          getOffsetAsQPoint();
        void            setOffset( const Data::DataIndex& offset ) { offset_ = offset; }
        void            setOffset( const QPoint& offset )
         { offset_.j = offset.x(); offset_.i = offset.y(); }

        cv::Mat*    getLabels() { return labels_; }
        void        setLabels( cv::Mat* labels ) { labels_ = labels; }
    };
protected:
    cv::Mat*        image_;
    OffsetOperator  offset_operator_;
    int             label_;
    uint            dimention_;

public:
    Data( cv::Mat* const    selection=0,
                            cv::Mat* const  labels = 0,
                            const uint      dimention=5,
                            const uint      i=0,
                            const uint      j=0,
                            const QPoint&   offset = QPoint( 0, 0 )
        );

    virtual ~Data();
    virtual double    operator []( uint index ); // get the component of vector(point)
    virtual DataIndex operator ()( uint index ) = 0;
    virtual DataIndex operator ()( uint i, uint j ) = 0;

    virtual cv::Mat*  getStatistics() { return 0; }             // get the centriod, centroid here - is a vector,  because vector is a cluster with size == 1
    virtual void      setCoordinates( int i, int j ) { } // set new Coordinatees
    virtual void      setCoordinates( const DataIndex& index ) {}
    virtual DataIndex getCoordinates() {}

    virtual int  getLabel() { return label_; }
    virtual void setLabel( int label ) { label_ = label; }

    virtual void        setImage( cv::Mat* image ) { image_ = image; }
    virtual cv::Mat*    getImage() { return image_; }


    cv::Mat*    getLabels();
    void        setLabels( cv::Mat* labels );
};

typedef Data::OffsetOperator OffsetOperator;

class StoredData : public Data {
private:
    cv::Mat     statisitcs_;

public:
    StoredData( cv::Mat* const  selection=0,
                cv::Mat* const  labels = 0,
                const uint      dimention=5,
                const uint      i=0,
                const uint      j=0 );
    StoredData(  );

    double      operator []( uint index );
    DataIndex   operator ()( uint index ){ return DataIndex({-1,-1}); }
    DataIndex   operator ()( uint i, uint j ) { return DataIndex({ i, j }); }
    cv::Mat*    getCentroid() { return &statisitcs_; }
    double      getCentroidComponent( uint index );
    void        setCoordinates( int i, int j );
    DataIndex   getCoordinates();
    QRgb        getColor();
    void        setLabel( int label );
};

Data::DataIndex operator + ( Data::DataIndex& a, Data::DataIndex& b );
Data::DataIndex operator - ( Data::DataIndex& a, Data::DataIndex& b );

//#ifdef unix
//#endif

#endif // DATA_H
