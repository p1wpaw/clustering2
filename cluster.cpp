#include "cluster.h"
#include <limits>
#include <QDebug>
#include "core.h"

Cluster::Cluster( Cluster* parent,
                  cv::Mat * const image,
                  cv::Mat * const labels,
                  int label,
                  const QPoint& offset
                )
    : Data( image, labels, 9, 0, 0, offset ), label_( label ), parent_( parent )
{
//    OffsetOperator
    if ( getLabels() ) logCvMat( getLabels(), "Cluster.txt" );

    int current_label, max_label = -1;
    color_ = qrand();

    if ( getLabels() ) {
        for ( int i = 0; i <  getLabels()->rows; i++ ) {
            for ( int j = 0; j <  getLabels()->cols; j++ ) {
                current_label =  getLabels()->at<int>( i, j );
                if ( label_ ==  current_label ) {
                    DataIndex index = offset_operator_.indexFromOffset( i, j );
                    data_.push_back( index );
                }
                else {
                    if ( current_label > max_label && label_ == -1 ) {
                        max_label = current_label;
                        children_.push_back( new Cluster(   this,
                                                            image,
                                                            getLabels(),
                                                            current_label,
                                                            offset_operator_.getOffsetAsQPoint()
                                                         ) );
                        return;
                    }
                }
            }
        }
    }

    average_ = this;
    if ( !parent_ ) calculateStatistics( this );
}

Cluster::Cluster( Cluster* parent, cv::Mat* const image, const QString& file_name_ )
    : Data( image ), parent_( parent )
{
}

Cluster::Cluster( ClusterPtrList* children ) {
    initializeFromChildren( children );
}

Cluster::~Cluster( ) {
//    if ( average_ != this ) delete average_;
}

void Cluster::initializeFromChildren( ClusterPtrList* children ) {
    children_ = *children;
}

double Cluster::operator []( uint index ) {
    return statistics_.at<double>( 0, index );
}

Data::DataIndex Cluster::operator ()( uint index ) {
    return data_[ index ];
}

Data::DataIndex Cluster::operator ()( uint i, uint j ) {
    for ( int index = 0; index < data_.size(); i++ ) {
        if ( data_[index].i == i &&data_[index].j == j ) {
            return data_[index];
        }
    }
    return DataIndex({ 0, 0 });
}

Cluster::DataIndex& Cluster::appendData( const DataIndex& value ) {}
void Cluster::removeFromData( uint index ) {}
void Cluster::removeFromData( uint i, uint j ) {}

Cluster&    Cluster::appendChild( Cluster* cluster ) {
    children_.push_back( cluster );
}
void        Cluster::removeChild( uint index ) {
    // for list children
    ClusterPtrList::iterator it = children_.begin();
    std::advance( it, index );

    if ( it != children_.end() ) {
        delete (*it);
        children_.remove( *it );
    }
}

Cluster* Cluster::childAt( uint index ) {
    if ( index > children()->size() ) return 0;
    ClusterPtrList::iterator it = children_.begin();
    std::advance( it, index );

    return *it;
}

Cluster * Cluster::difference( Cluster *a, Cluster* b ) {}

void Cluster::readCluster( const QString& file_name ) {}
void Cluster::saveCluster( const QString& file_name ) {}

//void Cluster::calculateCentroid(  Cluster* cluster, cv::Mat* statistics  ) {
//    int size = data_.size();
//    statistics_.at<double>( 0, 0 ) = statistics_.at<double>( 0, 1 ) = 0;
//    int color = 0;
//    for ( int i = 0; i < size; i++ ) {
//        statistics_.at<double>( 0, 0 ) += data_[i].i;
//        statistics_.at<double>( 0, 1 ) += data_[i].j;
//        color = image_->at<uint>( data_[i].i, data_[i].j );
//        for( int j = 0; j < 3; j++)
//           statistics_.at<double>( 0, j + 2 ) += rgbVecComponent( color, j );
//    }
//    statistics_.at<double>( 0, 5 ) =   (  statistics_.at<double>( 0, 2 )
//                                        + statistics_.at<double>( 0, 3 )
//                                        + statistics_.at<double>( 0, 4 )
//                                       ) / 3;



//}
//void    Cluster::calculateSqare( Cluster* cluster, cv::Mat* statistics ) {
//    static int child_count = 0;
//    statistics->at<double>( 0, 6 ) += data_.size();

//    ClusterPtrList::iterator it = children_.begin();

//    statistics->at<double>( 0, 6 ) /= child_count;
//}
//void    Cluster::calculatePerimeter() {
//    static int child_count = 0;
//    uint size = data_.size(), perimeter = 0;
//    int i, j;
//    DataIndex  labels_index;
//    cv::Mat* labels = getLabels();

//    for ( int k = 0; k < size; k++ ) {
//         labels_index = offset_operator_.offsetFromIndex( data_[ k ].i, data_[ k ].j );

//        i =  labels_index.i;
//        j =  labels_index.j;

//        if ( labels->at<uint>( i, ( ( j - 1 <= 0 ) ? j : j - 1 ) ) != label_ )
//            perimeter++;
//        else if ( labels->at<uint>( i, ( j + 1 >= labels->rows ) ? j : j + 1 ) != label_ )
//            perimeter++;
//        else if ( labels->at<uint>( ( i - 1 <= 0 ? i : i - 1), j ) != label_ )
//            perimeter++;
//        else if ( labels->at<uint>( ( i + 1 >= labels->cols ? i : i + 1), j ) != label_ )
//            perimeter++;
//    }

//    for( it; it != children_.end(); ++it )
//        calculateSqare( *it, statistics );

//    child_count++;

//    statistics->at<double>( 0, 7 ) += perimeter;
//}
//void    Cluster::calculateCompactness( Cluster* cluster, cv::Mat* statistics ) {
//    statistics_.at<double>( 0, 8 ) = pow(   statistics_.at<double>( 0, 7 ), 2 )
//                                            / statistics_.at<double>( 0, 6 ) ;
//    // perimeter ^ 2 / square
//}
//void    Cluster::calculateStatistics( Cluster* cluster, cv::Mat* statistics ) {
//    statistics_.create( 1, 9, cv::DataType<double>::type );
//    calculateCentroid( this, statistics );
//    calculateSqare( this, statistics );
//    calculatePerimeter( this, statistics );
//    calculateCompactness( this, statistics );
//}
