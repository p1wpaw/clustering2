#ifndef MEMTEST_H
#define MEMTEST_H

#include <QObject>

class MemoryTest : public QObject
{
    Q_OBJECT
public:
    explicit    MemoryTest( QObject *parent = 0 );
    ulong       max_alloc_size_;

    template< template <typename>   class StdContainer,
              typename              Type >
    uint
    maxStdContainerSize( StdContainer<Type> * container )  { return 0; }

signals:
    ulong       checkMaxAllocSize();



public slots:
    
};

#endif // MEMTEST_H
