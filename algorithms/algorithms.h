#ifndef ALGORITHMS_H
#define ALGORITHMS_H

#include "opencv2/core/core.hpp"
#include "core.h"
#include "cluster.h"

class Selection;
class FrameSelection;
typedef std::set<Selection*> SelectionPtrSet;

void                            clustersWrap(Cluster* node);
bool                            K_MeansStop(std::vector<Data*>* centers, std::vector<Data>* prev_centers);
void                            nearestPointMethod(float (*d)(Data *, Data *, std::vector<Data*>* points), std::vector<Data*>* points);
//template<template<typename TData>, TContainer>

//void                            K_Means2(int K, float (*d)(Data *, Data *), std::vector<Data*>* points);
void                            K_Means_pp(int K, std::vector<Data*>* points, std::vector<Data*>* centers, float (*d)(Data* a, Data* b));
void                            ISODATA(float  dt, float (*d)(Data *, Data *), std::vector<Data*> * points);
int                             serialScan(Selection* selecion, cv::Mat * image, cv::Mat * labels, byte epsilon1 = 0, byte epsilon2=255);
int                             test (Selection* selecion, cv::Mat * image, cv::Mat * labels, byte epsilon1 = 0, byte epsilon2=255);
//Cluster*                        clusterTreeRoot(std::vector<Data*>*  points);
//Cluster*                        makeCluster(std::vector<Data*>*  points, int m);
//void                            makeNormalCollision(EqTable & eq_table, int key);
//int                             findSimilarSegments(std::vector<std::pair<float, float> > * tresholds,
//                                                    std::vector<Cluster*>*points
//                                                    );
//std::vector<std::pair<float, float> > valuesMinMax(std::vector<Cluster *> *segments);

#endif // ALGORITHMS_H
