#ifndef ALGORITHMDIALOG_H
#define ALGORITHMDIALOG_H

#include <QDialog>
#include <QAbstractTableModel>
#include <unordered_map>
#include <QString>
#include "project.h"
#include "tablealgorithmmodels.h"

typedef std::unordered_map<std::string, QAbstractTableModel*> ModelsMap;
typedef std::pair<std::string, QAbstractTableModel*> ModelPair;


namespace Ui {
class AlgorithmDialog;
}

class AlgorithmDialog : public QDialog
{
    Q_OBJECT

private:
    Ui::AlgorithmDialog *ui;
    ModelsMap d_func_arg_models_;
    uint dimention_;
    Project * project_;

public:
    explicit AlgorithmDialog( QWidget *parent = 0, uint dimention = 5 );
    ~AlgorithmDialog();
    Ui::AlgorithmDialog * const getUi() { return ui; }
    void initializeDFuncTable();

    void    setDimention( uint dimention ) { dimention_ = dimention; }
    uint    getDimention() { return dimention_; }

    void            setProject( Project* const project ) { project_ = project; }
    Project* const  getProject() { return project_; }
};

#endif // ALGORITHMDIALOG_H
