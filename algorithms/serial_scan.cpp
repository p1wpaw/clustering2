#include "algorithms.h"
#include "frame.h"
#include "data.h"
#include <map>

typedef std::multimap<int, int> EqTable;

int serialScan(Selection* selection, cv::Mat* image, cv::Mat * labels, byte epsilon1, byte epsilon2) {
    QRect area = ( static_cast<FrameSelection*>( selection ) )->getArea();
    int x, y, i, j, label = 0,
            w = area.width(),
            h = area.height();
//    float t = begin - treshold;
    bool binA;
//    for (int i = 0; i < image->rows; i++)
//        for (int j = 0; j < image->cols; j++) {
////                Data & pt = (*image)[y * image->height() +x];
//            Data pt = img_index.at<Data>( i , j );
//        }
    OffsetOperator  offset_operator( labels, area.topLeft() );
    Data::DataIndex img_index({ 0, 0 });
    CvMatIndex      cv_mat_index( image );

    StoredData A ( image, labels, 5, 0, 0  ),
               B ( image, labels, 5, 0, 0 ),
               C ( image, labels, 5, 0, 0 );

    for ( i = area.y(); i < area.y() + h; i++) {
         for ( j = area.x(); j < area.x() + w; j++) {
            int bj = ( j - 1 < area.x() ) ? j : j - 1,
                ci = ( i - 1 < area.y() ) ? i : i - 1;

            A.setCoordinates( i, j );
//                binA = (pixelBrightness(A.pixelInt()) >= epsilon1 && pixelBrightness(A.pixelInt()) <= epsilon2);

            img_index = offset_operator.indexFromOffset( i, j );
            QRgb pix = image->at<QRgb>( img_index.i, img_index.j );

            binA = (pixelBrightness( pix ) >= epsilon1 &&
                    pixelBrightness( pix ) <= epsilon2    );

            if (!binA) continue;

            B.setCoordinates( i, bj );
            C.setCoordinates( ci, j );

//            B.setLabel( ( ( bj == j ) ? 0 : B.getLabel() ) );
//            C.setLabel( ( ( ci == i ) ? 0 : C.getLabel() ) );

            B.setLabel( ( ( bj == j ) ? 0 : labels->at<uint>( i,  bj ) ) );
            C.setLabel( ( ( ci == i ) ? 0 : labels->at<uint>( ci, j  ) ) );

            if      ( !B.getLabel() && !C.getLabel() ) A.setLabel( ++label );
            else if (  B.getLabel() && !C.getLabel() ) A.setLabel( B.getLabel() );
            else if ( !B.getLabel() &&  C.getLabel() ) A.setLabel( C.getLabel() );
            else if (  C.getLabel() &&  B.getLabel() ) {
                if ( C.getLabel() == B.getLabel() ) {
                    A.setLabel( B.getLabel() );
                    labels->at<int>( i, j ) = B.getLabel();
                }
                else {
                    A.setLabel( C.getLabel() );
                    labels->at<int>( i, j ) = C.getLabel();

                    // unite segments
                    for ( int k = area.y(); k < area.y() + h; k++ )
                        for ( int l = area.x(); l < area.x() + w; l++ )
                            if( labels->at<int>( k, l ) == B.getLabel() )
                                cv_mat_index.at<int>( k, l ) =  C.getLabel();
                }
            }
        }
    }
    return label;
}

int test( Selection *selecion, cv::Mat *image, cv::Mat *labels, byte epsilon1, byte epsilon2) {
//    OffsetOperator  offset_operator( labels, area.topLeft() );
//    Data::DataIndex img_index({ 0, 0 });
//    CvMatIndex index( labels );

//    StoredData A ( image, labels, 5, 0, 0  );
//    int w = area.width(), h = area.height();

    for ( int i = 0; i < labels->rows; i++) {
        for ( int j = 0; j < labels->cols; j++) {
//            A.setCoordinates( i, j );
//            A.setLabel( 1 );
//            index.at<uint>( i, j ) = 1;
            labels->at<int>( i, j ) = 1;
        }
    }
    logCvMat( labels, "test.txt");
    return 1;
}
