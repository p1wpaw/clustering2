#ifndef TABLEALGORITHMMODELS_H
#define TABLEALGORITHMMODELS_H

#include <QAbstractTableModel>
#include <QString>
#include <unordered_map>
#include <opencv2/core/core.hpp>
#include "distancefunctons.h"


class EuclideanDistanceArgTableModel : public QAbstractTableModel
{
private:
    cv::Mat data_;
    int dimention_;

public:
    EuclideanDistanceArgTableModel( uint dimention = 4 );

    int rowCount( const QModelIndex &parent = QModelIndex() ) const ;
    int columnCount( const QModelIndex &parent = QModelIndex() ) const;
    QVariant data( const QModelIndex &index, int role = Qt::DisplayRole ) const;
    bool setData( const QModelIndex & index, const QVariant & value, int role = Qt::EditRole);
    Qt::ItemFlags flags( const QModelIndex & index ) const ;
};

typedef std::map<QString, DistanceFuncton> DFunctionsMap;

//class DistanceFunctionDispatcher {
//private:
//    DFunctionsMap d_functions_map_;

//public:
//    DistanceFunctionDispatcher();
//    const DFunctionsMap& getDFunctionsMap ()
//        { return d_functions_map_;  }
//    void setDFunctionsMap( const DFunctionsMap& d_functions_map_ )
//        { d_functions_map_ = d_functions_map; }

//};

#endif // TABLEALGORITHMMODELS_H
