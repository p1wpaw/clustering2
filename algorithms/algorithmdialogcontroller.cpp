
#include "algorithmdialogcontroller.h"
#include "ui_doubleslider.h"
#include "cluster_tree/clustertreewidget.h"
#include "algorithmdialog.h"

AlgorithmDialogController::AlgorithmDialogController( QObject *tool_bar ) :
    QObject(tool_bar),
    tool_bar_( qobject_cast<QToolBar*>( tool_bar ) ),
    segmentation_dialog_    ( 0 ),
    clustering_dialog_      ( 0 ),
    alg_io_controller_      ( 0 )

{
    QWidget * p = 0;
    if ( tool_bar_ ) p = static_cast<QWidget*>( tool_bar_->parent() );
    segmentation_dialog_ = new SegmentationDialog( p );
    clustering_dialog_   = new AlgorithmDialog( p );

    QAction*  segmentation_action =  new QAction(QIcon(":/icons/segmentation.png"), tr("�����������"), tool_bar_ );
    tool_bar_->addAction( segmentation_action );
    connect(  segmentation_action, SIGNAL( triggered() ), SLOT( showSegmentationDialog() ) );
    connect(  segmentation_dialog_,SIGNAL  ( accepted() ), this, SLOT    ( runSegmentation() ) );

    QAction*  clustering_action =    new QAction(QIcon(":/icons/clustering.png"), tr("�������������"), tool_bar_ );
    tool_bar_->addAction( clustering_action );
    connect(  clustering_action, SIGNAL( triggered() ), SLOT( showClusteringDialog() ) );

    QAction*  cluster_tree_action =    new QAction(QIcon(":/icons/tree.png"), tr("������ ���������"), tool_bar_ );
    tool_bar_->addAction( cluster_tree_action );
    connect(  cluster_tree_action, SIGNAL( triggered() ), SLOT( showClusterTreeWidget() ) );
}

AlgorithmDialogController::~AlgorithmDialogController() {
    if ( segmentation_dialog_ ) delete segmentation_dialog_;
    if ( clustering_dialog_   ) delete clustering_dialog_;
}

void AlgorithmDialogController::showClusteringDialog() {
    clustering_dialog_->show();
}

void AlgorithmDialogController::showSegmentationDialog() {
    segmentation_dialog_->show();
}

void AlgorithmDialogController::showClusterTreeWidget() {
    cluster_tree_widget_->show();
}

void AlgorithmDialogController::runSegmentation() {
    DoubleSlider * double_slider = segmentation_dialog_->getUi()->double_slider_;
//    byte lower_brightness =
//            intToByte( double_slider->getUi()->line_edit_lower_->text().toInt() ),
//         upper_brightness =
//            intToByte( double_slider->getUi()->line_edit_upper_->text().toInt() );

//    SerialScanIO::SerialScanInput input;
    SelectionPtrSet* selections = project_->modelStorage()->getSelectionPtrSet();
    SelectionPtrSet::iterator it = selections->begin();

    for ( it; it != selections->end(); ++it ) {
        SerialScanIO::SerialScanInput input {
            project_->modelStorage()->getCvImage(),
            intToByte( double_slider->getUi()->slider_->lowerValue() ),
            intToByte( double_slider->getUi()->slider_->upperValue() ),
            ( *it )
        };


        SerialScanIO* sscan_io = new SerialScanIO( alg_io_controller_, &input );
        if ( error_handle( !sscan_io,
                            err_text( "ERR_BAD_ALLOC" ) +
                            " ��� �����\������ ���������: " +
                            sscan_io->key() )
           ) return;

        alg_io_controller_->insertIO( sscan_io );
    }
    alg_io_controller_->run( QRegExp( "sscan.*" ) );
}

void AlgorithmDialogController::setProject( Project *project ) {
   project_ = project;
   clustering_dialog_->setProject( project_ );
   segmentation_dialog_->setProject( project_ );
   if ( !project_ ) return;
   alg_io_controller_ = project_->modelStorage()->algorithmIOController();
}
