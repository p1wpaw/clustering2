#include "patt_recog_alg_io.h"
#include "algorithms.h"
#include "frame.h"
#include <opencv2/core/core.hpp>

const QString SerialScanIO::key_format_ = ( "sscan %1,%2 %3 -s %4" ); // -s is selection

SerialScanIO::SerialScanIO( QObject *parent, SerialScanInput* input ) :
    AlgorithmIO( parent )
{
    initialize( input );
}

//SerialScanIO::SerialScanIO( QObject *parent, Project* project, SerialScanInput& input ) :
//    AlgorithmIO( parent, project )
//{
//    initialize( &input );
//}

void SerialScanIO::initialize(SerialScanInput *input) {
    mutex_.lock();
    input_.image_               = input->image_;
    input_.lower_brightness_    = input->lower_brightness_;
    input_.upper_brightness_    = input->upper_brightness_;
    input_.selection_           = input->selection_;

    QRect& area = static_cast<FrameSelection*>( input_.selection_ )->getArea();
    int h_l = area.width(), w_l;

    output_.labels_.create( area.height(), area.width(), cv::DataType<int>::type );

    for ( int i = 0; i < output_.labels_.rows; i++ )
         for ( int j = 0; j < output_.labels_.cols; j++ )
             output_.labels_.at<int>( i, j ) = -1;
    mutex_.unlock();

//    output_.labels_ = new cv::Mat( input_.image_->rows,
//                                   input_.image_->cols,
//                                   input_.image_->type() );
}

SerialScanIO::~SerialScanIO() {
}

void SerialScanIO::run() {
    mutex_.lock();
//    output_.label_count_ = serialScan(
//                                input_.selection_,
//                                input_.image_,
//                                &( output_.labels_ ),
//                                input_.lower_brightness_,
//                                input_.upper_brightness_
//                             );

    output_.label_count_ = test(
                                input_.selection_,
                                input_.image_,
                                &( output_.labels_ ),
                                input_.lower_brightness_,
                                input_.upper_brightness_
                             );

    QPoint segment_offset = static_cast<FrameSelection*>( input_.selection_ )
                                    ->getArea().
                                    topLeft();

    output_.root_cluster_ = new Cluster( 0,
                                         input_.image_,
                                         &( output_.labels_ ),
                                         -1,
                                         segment_offset );
    mutex_.unlock();

//    for ( int i = 0; i < label_count_; i++ )
//        output_.root_cluster_->appendChild( new Cluster(    output_.root_cluster_,
//                                                            input_.image_,
//                                                            output_.labels_,
//                                                            i ) );


    QCoreApplication::postEvent( parent(), new AlgorithmCompleteEvent( this ) );
}

const QString SerialScanIO::key() {
    QString selection_represent;

    if ( input_.selection_->type() == Selection::FrameSelectionType ) {
        QRect area = static_cast<FrameSelection*>( input_.selection_ )->getArea();
        selection_represent = q_objQStringRepresent<QRect>( area );
    }

    return  ( key_format_
                .arg( byteToInt( input_.lower_brightness_ ) )
                .arg( byteToInt( input_.upper_brightness_ ) )
                .arg( byteToInt( 0 ) )
                .arg( selection_represent )
            );
}

void SerialScanIO::startAlgorithm() {}

void SerialScanIO::stopAlgorithm() {}

void SerialScanIO::saveToDB() {}

void SerialScanIO::getFromDB() {}
