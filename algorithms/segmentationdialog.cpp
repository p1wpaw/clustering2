#include "ui_segmentationdialog.h"
#include "segmentationdialog.h"


SegmentationDialog::SegmentationDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SegmentationDialog)
{
    ui->setupUi(this);
    ui->double_slider_->setMaximum( 255 );
    ui->double_slider_->setMinimum( 0 );
//    connect( this, SIGNAL(accepted()), this, SLOT( runSerialScan() ) );
}

SegmentationDialog::~SegmentationDialog()
{
    delete ui;
}
