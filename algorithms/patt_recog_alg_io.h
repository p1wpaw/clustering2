#ifndef PATT_RECOG_ALG_IO_H
#define PATT_RECOG_ALG_IO_H

#include "algorithmio.h"
#include "cluster.h"

class Selection;

// Serial Scan Algorithm input/output dispatcher
// provides storage i/o for using in IOController
class SerialScanIO : public AlgorithmIO {
public:
    typedef struct {
        cv::Mat*        image_;
        byte            lower_brightness_,
                        upper_brightness_;

        Selection*      selection_;
    } SerialScanInput;

    typedef struct {
        cv::Mat         labels_;
        Cluster*        root_cluster_;
        uint            label_count_;
    } SerialScanOutput;

    SerialScanInput     input_;
    SerialScanOutput    output_;
private:
    void    initialize( SerialScanInput* input = 0 );
protected:


    const static QString key_format_; // "sscan %1,%2 %3 -s %4

public:
//    void    setSelectionPtrSet( SelectionPtrSet* selection_set ) {
//        selection_set_ = selection_set;
//    }
//    SelectionPtrSet * getSelectionPtrSet() { return selection_set_; }

    SerialScanIO( QObject* parent = 0,
                  SerialScanInput* input = 0 );

//    SerialScanIO( QObject* parent = 0,
//                  Project* project = 0,
//                  const SerialScanInput& input );


    ~SerialScanIO();

    SerialScanInput*  const input()     { return &input_;  }
    SerialScanOutput* const output()    { return &output_; }

    void    setSelection( Selection* selection ) { input_.selection_ = selection; }
    Selection* getSelection() { return input_.selection_; }

    void setBrightness( const std::pair<byte, byte>& brigntness_interval );
    std::pair<byte, byte> getBrigtness() {
                return
                std::pair<byte, byte>(
                    input_.lower_brightness_,
                    input_.upper_brightness_); }

    void        setImage( cv::Mat* image ) { input_.image_ = image; }
    cv::Mat*    getImage() { return input_.image_; }

    void    run();
//    void    stop();
//    operator QString () { return key(); }

    void saveToDB();
    void getFromDB();

    const QString key();
    operator QString () { return key(); }

    Cluster*    cluster() { return output_.root_cluster_; }
    cv::Mat*    image()   { return input_.image_; }
    cv::Mat*    labels()  { return &( output_.labels_ ); }
    uint        labelCount() { return output_.label_count_; }

public slots:
    void    startAlgorithm();
    void    stopAlgorithm();
};

#endif // PATT_RECOG_ALG_IO_H
