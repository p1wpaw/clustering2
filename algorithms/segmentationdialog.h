#ifndef SEGMENTATIONDIALOG_H
#define SEGMENTATIONDIALOG_H

#include <QDialog>
#include "project.h"

namespace Ui {
class SegmentationDialog;
}

class SegmentationDialog : public QDialog
{
    Q_OBJECT

private:
    Ui::SegmentationDialog *ui;
    Project* project_;

public:    
    explicit SegmentationDialog(QWidget *parent = 0);
    ~SegmentationDialog();

    Project* const getProject () { return project_;  }
    void setProject( Project* const project ) { project_ = project; }

    Ui::SegmentationDialog * const getUi() { return ui; }
//public slots:
//    void runSerialScan();
};

#endif // SEGMENTATIONDIALOG_H
