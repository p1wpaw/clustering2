#include "tablealgorithmmodels.h"
#include "errorhandler.h"

EuclideanDistanceArgTableModel::EuclideanDistanceArgTableModel( uint dimention )  :
    dimention_( dimention )
{
    data_ .create( 1, dimention_, CV_64F );
    for ( int i = 0; i < dimention_; i++ )
        data_.at<double>( i ) = 1.0;
}

//EuclideanDistanceArgTableModel::~EuclideanDistanceArgTableModel() {
//}

int EuclideanDistanceArgTableModel::rowCount( const QModelIndex &parent ) const {
    Q_UNUSED(parent);
    return 1;
}

int EuclideanDistanceArgTableModel::columnCount( const QModelIndex &parent ) const {
    Q_UNUSED(parent);
    return dimention_;
}

QVariant EuclideanDistanceArgTableModel::data( const QModelIndex &index, int role ) const {
    if ( index.isValid() ) {
        if ( role == Qt::DisplayRole ) return QString("%1").arg( data_.at<double>( index.column() ) );
        else return QVariant();
    }
}

bool EuclideanDistanceArgTableModel::setData(const QModelIndex & index, const QVariant & value, int role ) {
    if ( role == Qt::EditRole && index.isValid() ) {
        data_.at<double>( 0, index.column() ) = value.toDouble();
        return true;
    }
    else return false;
    return false;
}
Qt::ItemFlags EuclideanDistanceArgTableModel::flags( const QModelIndex & index ) const {
    if (!index.isValid())
        return Qt::ItemIsEnabled;

    return QAbstractTableModel::flags(index) | Qt::ItemIsEditable;
}

