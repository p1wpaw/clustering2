#include "algorithmio.h"
#include <QDebug>
#include <QtTest/QTest>
#include "cluster_tree/clustertreewidget.h"
#include <QtTest/QTest>
//________________________________________________________________________________
// AlgorithmIO implementations

const QString AlgorithmIO::key_format_ = QString("alg_io");

AlgorithmIO::AlgorithmIO( QObject *parent ) :
    QThread(parent)
{

}
AlgorithmIO::~AlgorithmIO() {
}

void AlgorithmIO::run() {}

//________________________________________________________________________________
// AlgorithmIOController implementations
AlgorithmIOController::AlgorithmIOController( Project* project )
    : project_( project ), max_active_thread_count_( 50 ), active_thread_count_( 0 ) {

}

AlgorithmIOController::~AlgorithmIOController() {
    IOMap::iterator it = io_map_.begin();
    for ( it; it != io_map_.end(); ++it ) delete it->second;
}

void AlgorithmIOController::insertIO( AlgorithmIO *io ) {
    io_map_.insert( IOPair( io->key(), io ) );
}

void AlgorithmIOController::customEvent( QEvent *event ) {
    qDebug() << "alg_io_ctrlr::event" << event->type();
    if ( event->type() == static_cast<QEvent::Type>( QEvent::User + 1 ) ) {
        qDebug() << "alg_io_ctrlr::alg_complete_event";
        AlgorithmCompleteEvent* alg_complete_ev =
                static_cast<AlgorithmCompleteEvent*>( event );

//        io_map_.insert( IOPair(
//                            alg_complete_ev->getIO()->key(),
//                            alg_complete_ev->getIO() ) );

        active_thread_count_--;
        finished_thread_count_++;

        qDebug() << active_thread_count_;

        if ( !active_thread_count_ ) {
            qDebug() << "cluster_tree_widget: " << cluster_tree_widget_;
            QApplication::postEvent( cluster_tree_widget_,
                                     new AllAlgorithmsCompleteEvent(
                                            this,
                                            io_map_.begin()->second
                                         )
                                    );
            qDebug() << "cluster_tree_widget: " << cluster_tree_widget_;
        }
    }
    return QObject::customEvent( event );
}


void AlgorithmIOController::run( const QRegExp &key_regex ) {
    IOMap::iterator it = io_map_.begin();
    active_thread_count_ = finished_thread_count_ = 0;

    for ( it; it != io_map_.end(); ++it ) {
        if ( key_regex.exactMatch ( it->second->key() ) ) {
            while ( active_thread_count_ >= max_active_thread_count_ )
                QTest::qSleep( 5000 );
            it->second->start();
            active_thread_count_++;
        }
    }


}
//________________________________________________________________________________
