#ifndef ALGORITHMIO_H
#define ALGORITHMIO_H

#include <QThread>
#include <QEvent>
#include <set>
#include <opencv2/core/core.hpp>
#include "data.h"
#include "core.h"

class Project;
class ClusterTreeWidget;

typedef unsigned char byte;

#define ALG_COMPLETE_EV     1001
#define ALL_ALG_COMPLETE_EV 1002

// Base functional for Algorithm input/output dispatcher
// provides storage i/o for using in IOController
class AlgorithmIO : public QThread
{
    Q_OBJECT
protected:
    QMutex mutex_;
    const static QString key_format_;

public:
    explicit AlgorithmIO( QObject *parent = 0 );
    virtual ~AlgorithmIO();

//    Project*    getProject() { return project_; }
//    void        setProject( Project * project ) { project_ = project; }

    operator QString () { return key(); }

    void    run();

    virtual Cluster*    cluster() = 0;
    virtual cv::Mat*    image()   = 0;
    virtual cv::Mat*    labels()  = 0;
    virtual uint        labelCount() = 0;

    virtual const QString key() { return key_format_; }

    virtual void saveToDB()  {}
    virtual void getFromDB() {}

    QMutex& mutex() { return mutex_; }

signals:
    
public slots:
    virtual void    startAlgorithm() = 0;
    virtual void    stopAlgorithm() = 0;
};

class AlgorithmIOController : public QObject {
public:
    typedef std::pair<QString, AlgorithmIO*>    IOPair;
    typedef std::map<QString, AlgorithmIO*>     IOMap;

private:
    IOMap   io_map_;
    uint    active_thread_count_,
            max_active_thread_count_,
            finished_thread_count_;

    Project*            project_;
    ClusterTreeWidget*  cluster_tree_widget_;

public:
    AlgorithmIOController( Project* project = 0 );
    ~AlgorithmIOController();

    IOMap* ioMap() { return &io_map_; }
    void insertIO( AlgorithmIO* io );
//    bool event( QEvent *event );
    void customEvent( QEvent * );
    void run( const QRegExp& key_regex );

    void setClusterTreeWidget( ClusterTreeWidget* widget )
        { cluster_tree_widget_ = widget; }
    ClusterTreeWidget* getClusterTreeWidget()
        { return cluster_tree_widget_; }
};

// Algorithm I/O  events
class AlgorithmCompleteEvent : public QEvent {
private:
    AlgorithmIO * io_;
public:
    AlgorithmCompleteEvent( AlgorithmIO * io ) :
        QEvent( static_cast<QEvent::Type>( QEvent::User + 1 ) ),
        io_( io )
    {}
    AlgorithmIO *   const getIO() { return io_; }
    void setIO( AlgorithmIO* io ) { io_ = io; }
};

class AllAlgorithmsCompleteEvent : public QEvent {
private:
    AlgorithmIOController * alg_io_controller_;
    AlgorithmIO*            first_;
    static const QEvent::Type type_ = static_cast<QEvent::Type>( ALL_ALG_COMPLETE_EV );

public:
    AllAlgorithmsCompleteEvent( AlgorithmIOController* alg_io_controller, AlgorithmIO* first ) :
        QEvent( static_cast<QEvent::Type>( ALL_ALG_COMPLETE_EV ) ),
        alg_io_controller_( alg_io_controller ) ,
        first_( first )
    {
    }
    AlgorithmIOController*   const getAlgorithmIOContoller() { return alg_io_controller_; }
    void setIO( AlgorithmIOController* alg_io_controller )
        { alg_io_controller_ = alg_io_controller; }

    AlgorithmIO* first() { return first_; }
};


#endif // ALGORITHMIO_H
