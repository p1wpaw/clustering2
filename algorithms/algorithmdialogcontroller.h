#ifndef ALGORITHMDIALOGCONTROLLER_H
#define ALGORITHMDIALOGCONTROLLER_H

#include <QObject>
#include <QToolBar>
#include <QIcon>
#include <QAction>
#include "project.h"
#include "algorithmdialog.h"
#include "segmentationdialog.h"
#include "ui_segmentationdialog.h"

class ClusterTreeWidget;

//QString::operator std::string( const QString & str ) { return str.toStdString(); }

class AlgorithmDialogController : public QObject
{
    Q_OBJECT
private:
    AlgorithmIOController * alg_io_controller_;
    AlgorithmDialog*        clustering_dialog_;
    SegmentationDialog*     segmentation_dialog_;
    ClusterTreeWidget*      cluster_tree_widget_;
    QToolBar*               tool_bar_;
    Project*                project_;


public:
    explicit AlgorithmDialogController( QObject *tool_bar = 0 );
    ~AlgorithmDialogController();

    QToolBar* const getToolBar() { return tool_bar_; }
    void setToolBar( QToolBar* const tool_bar ) { tool_bar_ = tool_bar; }

    AlgorithmDialog* const getClusteringDialog() { return clustering_dialog_; }
//    void setClusteringDialog( AlgorithmDialog* const algorithm_dialog ) { clustering_dialog_ = algorithm_dialog; }

    SegmentationDialog* const getSegmentationDialog() {
        return segmentation_dialog_;
    }
//    void setSegmentationDialog( SegmentationDialog* const segmentation_dialog ) {
//        segmentation_dialog_ = segmentation_dialog;
//        connect( segmentation_dialog_,  SIGNAL  ( accepted() ),
//                 this,                  SLOT    ( runSegmentation() )
//               );
//    }


    ClusterTreeWidget*  getClusterTreeWidget()
        { return cluster_tree_widget_; }
    void setClusterTreeWidget( ClusterTreeWidget* widget ) {
        cluster_tree_widget_ = widget;
        if ( alg_io_controller_ ) alg_io_controller_->setClusterTreeWidget( widget );
    }

    Project* getProject () { return project_;  }
    void setProject( Project* project );

    AlgorithmIOController* getAlgorithmIOController()
        { return alg_io_controller_; }
    void setAlgorithmIOController( AlgorithmIOController* alg_io_ctrlr )
        { alg_io_controller_ = alg_io_ctrlr; }


signals:
    
public slots:
    void showSegmentationDialog();
    void showClusteringDialog();
    void showClusterTreeWidget();
    void runSegmentation();
};

#endif // ALGORITHMDIALOGCONTROLLER_H
