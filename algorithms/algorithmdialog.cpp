#include "ui_algorithmdialog.h"
#include "algorithmdialog.h"


AlgorithmDialog::AlgorithmDialog(QWidget *parent, uint dimention ) :
    QDialog(parent),
    ui(new Ui::AlgorithmDialog),
    dimention_(dimention)
{
    ui->setupUi(this);
    initializeDFuncTable();
    ui->table_d_func_view_->setModel( d_func_arg_models_[ tr( "��������� ����" ).toStdString() ] );

    ModelsMap::iterator it = d_func_arg_models_.begin();
    int i = 0;
    for ( it; it != d_func_arg_models_.end(); ++it, i++ )
        ui->combo_box_d_func_->insertItem( i, QString::fromStdString( it->first ) );
}

AlgorithmDialog::~AlgorithmDialog()
{
    delete ui;
}

void AlgorithmDialog::initializeDFuncTable() {
    d_func_arg_models_.insert( ModelPair(
                                    tr("��������� ����").toStdString(),
                                    new EuclideanDistanceArgTableModel( dimention_ ) ) );
    d_func_arg_models_.insert( ModelPair(
                                    tr("���� ������������").toStdString(),
                                    new EuclideanDistanceArgTableModel( dimention_ ) ) );
}
