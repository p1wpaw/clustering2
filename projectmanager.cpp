#include "projectmanager.h"
#include "painter_contexts/paintercontroller.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QPushButton>

typedef std::pair<QString, Project*> ProjectPair;
typedef std::pair<ProjectsMap::iterator, bool> InsertResult;

ProjectManager::ProjectManager(MainWindow *parent) :
  QObject(parent), main_window_(parent) {

    connect( main_window_->getUi()->actionProjectCreate, SIGNAL( triggered() ),
              this, SLOT( createProject() ) );
    connect( main_window_->getUi()->actionProjectRead, SIGNAL( triggered() ),
              this, SLOT( readProject() ) );

}

ProjectManager::~ProjectManager() {}

void ProjectManager::createProject( QString& name, QString& file_name ) {}

//#define tab_widget main_window_->getUi()->tabWidget
void ProjectManager::createProject( ) {
    QString name =  "unnamed";
     // Prepae (Brut) default project name, because std::map is a unique-key store
    uint i = 1;
    main_window_->getUi()->tabWidget;
    for ( ProjectsMap::iterator it = projects_.find( name );
          it != projects_.end();
          it = projects_.find( name ) ) {
        i++;
        name = QString( "unnamed%1" ).arg( i );
    }

    // adjust project via ProjectDialog
    ProjectPair project_pair( name, new Project() );

    ProjectDialog project_dialog( (QWidget* )0, project_pair.second, project_pair.first  );
    if ( ! project_dialog.exec() ) {
        // Don't add project if user clicked "cancel" button
        delete project_pair.second;
        return;
    }
    project_dialog.adjustProject(); // set name and read image from file

    // insert ptoject to container
    QTabWidget * tab_widget = main_window_->getUi()->tabWidget;
    project_pair.first = project_dialog.getProjectName();
    projects_.insert( project_pair );

    // initialize painter widget and his controller
    Painter * painter = new Painter( tab_widget,
                                     project_pair.second->modelStorage()->getCvImage() );   // tab widget as painter parent, image
    painter->setProject( project_pair.second );

    PainterController * p_controller = main_window_->getPainterController();
    p_controller->setPainter( painter );
    p_controller->setCurrentProject( project_pair.second );
    main_window_->setPainterController( p_controller );


    // insert paiter to tab and tab set tab text as project name
    tab_widget->insertTab(
                tab_widget->count(),
                painter,
                project_pair.first ); // index, new Painter, project_names
    painter->show();

    current_project_name_ = project_pair.first;

    // adjust toolbar controllers
    main_window_->getAlgorithmDialogController()->setProject( project_pair.second );

    // adjust widgets and views
    main_window_->clusterTreeWidget()->setProject( project_pair.second );
}

void ProjectManager::deleteProject( QString& name ) {}

void ProjectManager::readProject( const QString& file_name ) {
    QFile file( file_name );
    if ( error_handle( !file.open( QIODevice::ReadOnly ),
         err_text("ERR_CONF_FILE_OPEN",  file_name ) ) )
        return;

    QDomDocument doc;
    parseXML( doc, file );
    QDomElement element = doc.elementsByTagName("project").at(0).toElement();
    QString name = element.attribute("name");

    Project* project = new Project( element, file_name );
    InsertResult insert_result = projects_.insert( ProjectPair( name, project ) );
    if ( error_handle( !insert_result.second, err_text( "ERR_ADD_PROJ" ) ) ) {
        delete project;
        return;
    }
    file.close();
}

void ProjectManager::readProject() {}

void ProjectManager::closeProject() {}

void ProjectManager::readConfig() {}

void ProjectManager::writeConfig() {}
