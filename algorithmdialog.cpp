//#include "ui_algorithmdialog.h"
//#include "algorithmdialog.h"
//#include "mainwindow.h"
//#include "ui_mainwindow.h"
//#include <QStandardItemModel>
//#include <QMessageBox>

////extern int _settings::data_dimention = 0;
////extern QImage * _settings::current_image = 0;

////Metrics::Points::Distance   Metrics::Points::D = 0;
////Metrics::Clusters::Distance Metrics::Clusters::D = 0;

//AlgorithmDialog::AlgorithmDialog(QWidget *parent, MainWindow * _mw) :
//    QDialog(parent), mainWindow(_mw),
//    ui(new Ui::AlgorithmDialog)
//{
//    ui->setupUi(this);
//    ui->tableWidget->verticalHeader()->hide();
//    ui->tableWidget->horizontalHeader()->hide();
//    ui->tableWidget->insertColumn(0);
//    ui->tableWidget->setColumnWidth(0, 205);
//    ui->tableWidget->insertColumn(1);
////    Metrics::Points::D = Metrics::Points::Euclidean;
////    Metrics::Clusters::D = Metrics::Clusters::Euclidean;
//    root = 0;

//    connect( ui->comboBox,   SIGNAL(currentIndexChanged(int)), this, SLOT(algrithmTableParam(int)) );
//    connect( ui->comboBox_2, SIGNAL(currentIndexChanged(int)), this, SLOT( measureTableParam(int)) );

//    ui->treeWidget->setColumnCount(2);
//    insertParam(ui->tableWidget, 0, tr("K (количество кластеров)"));
////    _settings::data_dimention = 2;
//}

//AlgorithmDialog::~AlgorithmDialog() {
//    delete ui;
//}

//void AlgorithmDialog::insertParam(QTableWidget * t_widget, int pos,  QString name, QString value) {
//    t_widget->insertRow(pos);
//    t_widget->setItem(pos, 0, new QTableWidgetItem(name, 1));
//    t_widget->setItem(pos, 1, new QTableWidgetItem(value, 1));
//}

//void AlgorithmDialog::algrithmTableParam(int index) {
////    for(int i = 0; i < 1; ++i)
//    ui->tableWidget->removeRow(0); //i
////    ui->tableWidget->removeColumn(0);
////    ui->tableWidget->removeColumn(1);

//    switch(index) {
//        case 0: case 1: case 3: default:
//    //        QMessageBox::information(this, "aa", "aa");
//            insertParam(ui->tableWidget, 0, tr("K (количество кластеров)"));
//            break;
//        case 2:
//            insertParam(ui->tableWidget, 0, tr("Dt (пороговое расстояние)"));
//            break;
//    }
//}

//void AlgorithmDialog::measureTableParam(int index) {
////    QTableWidget * mes_param = new QTableWidget(this);
////    mes_param->setRowCount(8);
////    while (!ui->gridLayout->isEmpty())
////        delete ui->gridLayout->takeAt(0)->widget();

////    ui->gridLayout->addWidget(mes_param, 0 ,0 , 1, 1, Qt::AlignTop);
////    switch (index) {
////        case 2:
////            if (_settings::context == Context::clusters)  {
////                insertParam(mes_param, 0, tr("kx"), "1.0");
////                insertParam(mes_param, 1, tr("ky"), "1.0");
////                insertParam(mes_param, 2, tr("kr"), "1.0");
////                insertParam(mes_param, 3, tr("kg"), "1.0");
////                insertParam(mes_param, 4, tr("kb"), "1.0");
////                insertParam(mes_param, 5, tr("kS"), "1.0");
////                insertParam(mes_param, 6, tr("kP"), "1.0");
////                insertParam(mes_param, 7, tr("kQ"), "1.0");
////            } else {
////                for (int i = 0; i < _settings::data_dimention; ++i) {
////                    insertParam(mes_param, i, QString::number(i), "1.0");
////                }
////            }
////            break;
////        default:
////            break;
////    }

//}

//void AlgorithmDialog::makeNode(Cluster *node, QTreeWidgetItem* parent, QTreeWidgetItem* root_item) {
//    QTreeWidgetItem * item;
//    if(parent)  item = new QTreeWidgetItem(parent);
//    else        { item = new QTreeWidgetItem(ui->treeWidget); item->setText(0, tr("Корень")); }

//    QString info = tr("Центр: ( x = %1, y = %2 ), Метка: %3").arg(QString::number(node->x()),
//                                                            QString::number(node->y()),
//                                                            QString::number(node->m));

//    item->setBackground( 0, QBrush(QColor(node->color)) );
//    if(node != root) item->setText(1, info);

//    std::vector<Cluster*>::iterator c_it = node->children.begin(), c_end = node->children.end();
//    std::vector<Data*>::iterator  p_it = node->points.begin(), p_end = node->points.end();

////    if (node != root) parent->addChild(item);

//    for(c_it; c_it != c_end; ++c_it) makeNode(*c_it, item, root_item);
//    for(p_it; p_it != p_end; ++p_it) makeNode(*p_it, item);
////    this->ui->treeWidget->header()->setResizeMode(QHeaderView::Interactive);
//}
//void AlgorithmDialog::makeNode(Data *node, QTreeWidgetItem* parent) {
//    QTreeWidgetItem * item = new QTreeWidgetItem(parent);
//    QString info = tr("x = %1, y = %2").arg(QString::number(node->x()), QString::number(node->y()));

//    item->setText(1, info);
//}

//void AlgorithmDialog::makeTree() {
////    QTreeWidgetItem * root_item = new QTreeWidgetItem(ui->treeWidget);
////    QString info = QString("Центр: ( x = %1, y = %2 ), Метка: %3").arg(QString::number(root->center.x),
////                                                           QString::number(root->center.y),
////                                                           QString::number(root->label));
////    vector<Cluster*>::iterator c_it = root->children.begin(), c_end = root->children.end();
//    makeNode(root, 0, 0);
//}

//void AlgorithmDialog::accept() {
////    if (root) { delete root; root = 0; }
//    int n, sel_dist = sel_dist = ui->comboBox_2->currentIndex();
//    QTableWidget * m_par;
//    std::vector<Data*> * points;
//    std::vector<Data*> * ct = NULL;
//    int K = 0;
//    switch (sel_dist) {
//        case 0:  // Евклидово расстояние
////            _settings::w_coeff.assign(_settings::data_dimention, 1);
//            Metrics::Points::D = &(Metrics::Points::Euclidean);
//            break;
//        case 1:
//            n = ui->gridLayout->rowCount();
//            m_par = static_cast<QTableWidget*>(ui->gridLayout->itemAt(0)->widget());
//            for (int i = 0; i < n; i++)
//                _settings::w_coeff[i] = m_par->item(i, 1)->text().toFloat();

//        case 2:
////            _settings::w_coeff.assign(_settings::data_dimention, 1);
//            Metrics::Points::D = &(Metrics::Points::Mahalanobis);
//            break;
//    }

//    if (_settings::context == Context::example) {
//        points = &(mainWindow->ui->paintWidget->points);
//        _settings::w_coeff.reserve(_settings::data_dimention);
//        _settings::w_coeff.assign(_settings::data_dimention, 1.0);

//        int select = ui->comboBox->currentIndex(), sel_dist = ui->comboBox_2->currentIndex();
//    //  mainWindow->ui->paintWidget->p_map->loadFroData(mainWindow->ui->paintWidget->backup, "PNG");


//        switch (select) {
//            case 0:  { // K-means - перерасчет после добавления точки
//                int K = ui->tableWidget->item(0,1)->text().toInt();
//                Algorithms::K_Means(K, Metrics::Points::D, &(mainWindow->ui->paintWidget->points));
//                break;
//            }
////            case 1: {
////                int K = ui->tableWidget->item(0,1)->text().toInt();
////                Algorithms::K_Means2(K, Metrics::Points::D, &(mainWindow->ui->paintWidget->points));
////                break;
////            }
//            case 2: {
//    //            Algorithms::nearestPointMethod(Metrics::Points::D, &(mainWindow->ui->paintWidget->points));
//                float Dt = ui->tableWidget->item(0,1)->text().toDouble();
//                Algorithms::ISODATA(Dt, Metrics::Points::D, &(mainWindow->ui->paintWidget->points));
//                break;
//            }
//            case 3: {
//                int K = ui->tableWidget->item(0,1)->text().toInt(), N = mainWindow->ui->paintWidget->points.size();
//    //            int * labels = new int[N];
//                cv::Mat data_(N, _settings::data_dimention, CV_32F), _l(N, 1, CV_32S);
//    //            for (int i = 1; i <= K; ++i) labels[i - 1] = i;
//                float cm;
//                std::vector<Data*>::iterator it = mainWindow->ui->paintWidget->points.begin();

//                int i = 0., dim = _settings::data_dimention;
//                for (it = mainWindow->ui->paintWidget->points.begin(); it != mainWindow->ui->paintWidget->points.end(); ++it, ++i) {
//                    for (int j = 0; j < dim; ++j)
//                        data_.at<float>(i, j) = (float )(**it)[j];
//                }
//                for (i = 0; i < N; i++)  { _l.at<int>(i, 0) = i % K; }
//                cv::Mat centers(K, _settings::data_dimention, CV_32F);
//                float x = cv::kmeans(data_, K, _l,
//                           cv::TermCriteria(cv::TermCriteria::EPS, 20, 10E-1),
//                           20,
//                           cv::KMEANS_PP_CENTERS + cv::KMEANS_USE_INITIAL_LABELS,
//                           centers);
//                for (it = mainWindow->ui->paintWidget->points.begin(), i = 0; it != mainWindow->ui->paintWidget->points.end(); ++it, ++i) {
//                    (*it)->m = _l.at<int>(i, 0) + 1;
//                }
//                break;
//            }
//        }
//        mainWindow->ui->paintWidget->mode = PaintMode::cluster;
//    } else {
//        if (_settings::context == Context::find_similar) {
//             ct = new std::vector<Data*>;
//             ct->push_back(mainWindow->average_cl);
//             K = 1;
//        }

////        _settings::points = static_cast<std::vector<Data*>*>(&(mainWindow->all_cl->children));
//        _settings::data_dimention = 8;
//        if( ! ui->gridLayout->isEmpty() ) {
//            m_par = static_cast<QTableWidget *>(ui->gridLayout->itemAt(0)->widget());
//            _settings::w_coeff.reserve(_settings::data_dimention);
//            for (int i = 0; i < _settings::data_dimention; i++)
//                _settings::w_coeff[i] = m_par->item(i, 1)->text().toFloat();
//        } else
//            _settings::w_coeff.assign(_settings::data_dimention, 1);

//        int select = ui->comboBox->currentIndex(), sel_dist = ui->comboBox_2->currentIndex();
////        mainWindow->ui->paintWidget->p_map->loadFroData(mainWindow->ui->paintWidget->backup, "PNG");
////        switch (sel_dist) {
////            case 0:  // Евклидово расстояние
////                Metrics::Points::D = &(Metrics::Points::Euclidean); break;
////            case 1:
////                Metrics::Points::D = &(Metrics::Points::Mahalanobis);  break;
////        }
//        resetLabels(_settings::points);
//        switch (select) {

//            case 0: {
//                if(!K) K = ui->tableWidget->item(0,1)->text().toInt();

//                Algorithms::K_Means(K, Metrics::Points::D, _settings::points, ct);
//                break; }
////            case 3:

////                K = ui->tableWidget->item(0,1)->text().toInt();
////                int N = mainWindow->ui->paintWidget->points.size();
////                cv::Mat data1(N, _settings::data_dimention, CV_32F), _l(N, 1, CV_32S);


////                int dim = _settings::data_dimention;
////                std::vector<Data*>::iterator it = mainWindow->all_cl->children.begin();

////                for (int i = 0; i < N; ++i) {
////                    for (int j = 0; j < dim; ++j) {
////                        data1.at<float >(i, j) = (float)(**it)[j];
//////                        () << QString::number((*it)[j]);
////                    }
////                    ++it;
////                }
////                for (int i = 0; i < N; i++)  { _l.at<int>(i, 0) = i % K; }
////                cv::Mat centers(K, _settings::data_dimention, CV_32F);
////                float x = cv::kmeans(data1, K, _l,
////                           cv::TermCriteria(cv::TermCriteria::EPS, 20, 10E-1),
////                           20,
////                           cv::KMEANS_PP_CENTERS + cv::KMEANS_USE_INITIAL_LABELS,
////                           centers);

////                it = mainWindow->ui->paintWidget->points.begin();
////                for (int i = 0; i < N; i++) {
////                    if (it == mainWindow->ui->paintWidget->points.end()) {
////                        continue;
////                    }
////                    (*it)->m = _l.at<int>(i, 0) + 1;
////                    ++it;
////                }
////                break;
//        }
//        if(ui->checkBox->isChecked()) {
//            mainWindow->ui->paintWidget->mode  = PaintMode::wrap;
//            Algorithms::clustersWrap(root);
//        } else
//        mainWindow->ui->paintWidget->mode  = PaintMode::image_clusters;
//    }

//    root = Algorithms::clusterTreeRoot(_settings::points);
//    if(ui->checkBox->isChecked()) {
//        if (_settings::data_dimention == 2) mainWindow->ui->paintWidget->mode  = PaintMode::wrap;
//        else mainWindow->ui->paintWidget->mode  = PaintMode::image_shells;
//        Algorithms::clustersWrap(root);
//    }
////    root = new Cluster(&(mainWindow->ui->paintWidget->points), 0,0,1);
//    makeTree();
//    delete ct; ct = NULL;
//    mainWindow->ui->paintWidget->cluster = root;
//    mainWindow->ui->paintWidget->update();
//}
