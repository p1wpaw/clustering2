#ifndef ERRORHANDLER_H
#define ERRORHANDLER_H

#include <QString>
#include <QMessageBox>
#include <QFile>
#include <QObject>
#include <QApplication>
#include <QDebug>
#include <map>

class ErrorHandler : public QObject
{
    Q_OBJECT
private:
    static std::map<QString, QString> errors_;
//    static void insert(QString key, QString text) { errors_.insert(key, value); } // REM: possibly use reference to QString
public:
    static QString   getErrorText(const QString& key);
    static QString   getErrorText(const QString& key, const QString& extra);

    static void initialize();
public:
    explicit ErrorHandler(QObject *parent = 0);
    
signals:
    
public slots:
    
};

bool error_handle( bool error_condition, const QString &text, QString * buffer = 0);

#define err_text ErrorHandler::getErrorText

#endif // ERRORHANDLER_H
