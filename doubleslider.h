#ifndef DOUBLESLIDER_H
#define DOUBLESLIDER_H

#include <QWidget>
#include <QxtSpanSlider>

namespace Ui {
class DoubleSlider;
}

class DoubleSlider : public QWidget
{
    Q_OBJECT
    
public:
    explicit DoubleSlider(QWidget *parent = 0);
    explicit DoubleSlider(QWidget *parent, int min, int max);
    ~DoubleSlider();
    Ui::DoubleSlider* getUi() { return ui; }
    QxtSpanSlider* spanSlider();

    
private:
    Ui::DoubleSlider *ui;

public slots:
    void setLineEditLowerValue( int value );
    void setLineEditUpperValue( int value );

    void setMinimum();
    void setMaximum();
    void setMinimum( int min );
    void setMaximum( int max );


};

#endif // DOUBLESLIDER_H
