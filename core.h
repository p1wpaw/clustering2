#ifndef CORE_H
#define CORE_H

#include <QRgb>
#include <QString>
#include <QDomDocument>
#include <QFile>
#include "data.h"
#include "errorhandler.h"
#include "cvmatindex.h"


// Color tools
typedef uchar byte;

uint                    byteToInt( byte val );
byte                    intToByte( uint val );
uint                    rgbVecComponent(QRgb rgb, uint i);
byte                    pixelBrightness( uint pixel );
bool                    setQPainterPenColor( QPainter * const painter, QRgb rgb );
QRgb                    randomRgb();
// XML tools
bool                    parseXML( QDomDocument& doc, QFile & file );

// CvMat <-> QImage Convert tools
QImage      cvMatToQImage(  const cv::Mat& inMat );
QPixmap     cvMatToQPixmap( const cv::Mat& inMat );
cv::Mat     QImageToCvMat(  const QImage& inImage,
                            bool  inCloneImageData = true );
cv::Mat     QPixmapToCvMat( const QPixmap& inPixmap,
                            bool  inCloneImageData = true );

template <typename Type > void fillCvMat( cv::Mat* mat, const Type& value ) {
    CvMatIndex index( mat );
    for ( int i = 0; i  < mat->rows; i++ ) {
        for ( int j = 0; j < mat->cols; j++ ) {
            index.at<Type>( i, j ) = 0;
        }
    }
}

// string represent tools
template < typename T > QString q_objQStringRepresent( const T& obj ) {
    QString     string;
    QDebug      stream( &string );

    stream << obj;

    return string;
}

void logCvMat( cv::Mat* mat, QString file_name );
QString cvMatQStringRepresent( cv::Mat* mat );
class Cluster;
QString clusterDataQStringRepresent( Cluster* cluster, uint index );

#endif // CORE_H
