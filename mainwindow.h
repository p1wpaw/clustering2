#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "painter_contexts/paintercontroller.h"
#include "algorithms/algorithmdialogcontroller.h"
#include "cluster_tree/clustertreewidget.h"

//#include "projectmanager.h"

class ProjectManager;

namespace Ui {
class MainWindow;
}


class MainWindow : public QMainWindow
{
    Q_OBJECT

private:
//    QDomDocument config_;
//    QString config_file_name_;
//    ProjectDialog * project_dialog_;
    ProjectManager* project_manager_;
    PainterController * painter_controller_;
    AlgorithmDialogController * alg_dialog_controller_;
    Ui::MainWindow *ui;
    ClusterTreeWidget * cluster_tree_widget_;
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    uint readConfig();
    void initializeProjectManager();

    Ui::MainWindow* const getUi() { return ui; }
    void createToolBars();

    PainterController * getPainterController()
        { return painter_controller_; }
    void                setPainterController( PainterController* painter_controller )
        { painter_controller_ = painter_controller; }

    AlgorithmDialogController * getAlgorithmDialogController()
        { return alg_dialog_controller_; }
    void setAlgorithmDialogController( AlgorithmDialogController* alg_dialog_controller )
        { alg_dialog_controller_ = alg_dialog_controller; }

    ClusterTreeWidget * clusterTreeWidget() { return cluster_tree_widget_; }
};

#endif // MAINWINDOW_H
