#include "clusterstatistics.h"
#include "cluster.h"
#include "distancefunctons.h"

void ImageClusterStatistics::calculateStatistics( Cluster* cluster ) {
    statistics_ = cv::Mat::zeros( 1, Size, cv::DataType<double>::type );
    count_ = { 0, 0 };

    count_ = calculate( cluster );

    for ( int j = 0; j < statistics_.cols; j++ ) {
        int n = ( j < SquareIndex ) ? count_.data : count_.children ;
        statistics_.at<double>( 0, j ) /= n;
    }
}

ClusterStatistics::Count ImageClusterStatistics::calculate( Cluster* cluster ) {
    int i;
    Count count { cluster->children()->size(), cluster->data()->size() };
    Cluster::ClusterPtrList::iterator it = cluster->children()->begin();

    for (; it != cluster->children()->end(); ++it ) {
        calculateCentroid   ( *it );
        calculateSquare     ( *it );
        calculatePerimteter ( *it );
        calculateCompactness( *it );
    }
}

void ImageClusterStatistics::calculateCentroid( Cluster *cluster ) {
    int datasize = cluster->dataSize();
    Cluster::DataIndexVec& data = *( cluster->data() );
    // color
    for( int i = 0; i < datasize; i++ ) {
        statistics_.at<double>( 0, RowIndex     ) += data[i].i;
        statistics_.at<double>( 0, ColomnIndex  ) += data[i].j;
        uint color = cluster->getImage()->at<uint>( data[i].i, data[i].j );

        double channel_intensity;

        for( int j = 0; j < 3; j++) { // BGR (RGB)
            channel_intensity += rgbVecComponent( color, j );
            statistics_.at<double>( 0, j + 2 ) += channel_intensity;
            statistics_.at<double>( 0, BrightnessIndex ) += channel_intensity;
        }
    }
    // brightness
    statistics_.at<double>( 0, BrightnessIndex ) /= 3;
}

void ImageClusterStatistics::calculateSquare( Cluster* cluster ) {
    int square = cluster->dataSize();
    statistics_.at<double>( 0, SquareIndex ) += square;
}

void ImageClusterStatistics::calculatePerimteter( Cluster *cluster ) {
    Cluster::DataIndexVec& data = *( cluster->data() );

    uint size = data.size(), perimeter = 0;

    int i, j;
    Data::DataIndex labels_index;
    cv::Mat& labels = *( cluster->getLabels() );

    for ( int k = 0; k < size; k++ ) {
        labels_index = cluster->offsetOperator().offsetFromIndex( data[ k ].i, data[ k ].j );

        i =  labels_index.i;
        j =  labels_index.j;

        if ( labels.at<int>( i, ( ( j - 1 <= 0 ) ? j : j - 1 ) ) != cluster->getLabel() )
            perimeter++;
        else if ( labels.at<int>( i, ( j + 1 >= labels.rows ) ? j : j + 1 ) != cluster->getLabel() )
            perimeter++;
        else if ( labels.at<int>( ( i - 1 <= 0 ? i : i - 1), j ) != cluster->getLabel() )
            perimeter++;
        else if ( labels.at<int>( ( i + 1 >= labels.cols ? i : i + 1), j ) != cluster->getLabel() )
            perimeter++;
    }

    statistics_.at<double>( 0, PerimeterIndex ) += perimeter;
}
void ImageClusterStatistics::calculateCompactness( Cluster *cluster ) {
    statistics_.at<double>( 0, CompactnessIndex ) =
            pow(    statistics_.at<double>( 0, PerimeterIndex ), 2 )
            /       statistics_.at<double>( 0, SquareIndex ) ;
}

//void ImageClusterStatistics::calculateDispersion( Cluster *cluster ) {
//    Cluster::DataIndexVec* data = cluster->data();
//    int size = data->size();
//    for( int i = 0; i < size; i++ ) {
//        statistics_.at<int>( 0, DispersionIndex ) += 0;
//    }
//}
