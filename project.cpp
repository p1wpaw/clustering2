#include "project.h"
#include <QRegExp>
#include <opencv2/highgui/highgui.hpp>
#include "modelstorage.h"

Project::Project() {
//    model_storage_.setProject( this );
    model_storage_ = new ProjectModelStorage(this);
}

Project::Project( const QString & name, const QString& abs_file_name ) {
//    model_storage_.setProject( this );
    model_storage_ = new ProjectModelStorage(this);
}

Project::Project( const QDomElement & dom, const QString& abs_file_name ) {
    // Read project configuration from prepared DOM element(tree)
    model_storage_ = new ProjectModelStorage(this);
    name_ = dom.attribute( "name" );
    QRegExp abs_file_name_regex("[A-Z]:\\.+");
    file_name_ = ( abs_file_name_regex.exactMatch(abs_file_name) ) ? file_name_ : QString::null;

    image_file_name_ = dom.elementsByTagName("image").at(0).toElement().attribute("src");
    loadImage( image_file_name_ );

//    model_storage_.setProject( this );

}

Project::~Project() {
//    model_storage_.setProject( this );
    delete model_storage_;
}

//void Project::setDistanceFunction( DistanceFunc distance_func = &(DistanceFuncton::euclidean) ) {}

void Project::loadImage() {}

void Project::loadImage( const QString& image_file_name ) {
    if ( image_file_name != QString::null ) {
        model_storage_->setCvImage(
                    cv::imread( image_file_name.toStdString(), CV_LOAD_IMAGE_COLOR ) );
        image_file_name_ = image_file_name;
    }
}
void Project::closeImage() {}
