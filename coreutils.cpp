#include <QDomDocument>
#include <QFile>
#include <QString>
#include <QRgb>
#include <QColor>
#include <QPainter>
#include <QTime>
#include "core.h"
#include "errorhandler.h"

// --- RGB Functions

byte pixelBrightness( uint pixel ) {
    byte    r = rgbVecComponent( pixel, 2 ), // (.3)  *
            g = rgbVecComponent( pixel, 1 ), // (.59) *
            b = rgbVecComponent( pixel, 0 ); // (.11) *

    return ( ( r + g + b ) / 3 );
}

uint byteToInt(byte val) {
    int a = 0;
    *((byte*)&a) = val;
    return a;
}

byte intToByte(uint val) {
    byte * p = (byte*)&(val);
    return *p;
}

uint rgbVecComponent(QRgb rgb, uint i) {
    if (i >= 4 && i < 0) return 0;
    byte * p = (byte*)(&rgb);
    p = p + i;
    return byteToInt(*p);
}

bool setQPainterPenColor( QPainter * const painter, QRgb rgb ) {
    if ( !painter ) return false;
    QColor  color( rgb );
    QPen    pen( color );

    color.setRgb( rgb );
    pen.setColor( color );
    painter->setPen( pen );
}

QRgb randomRgb() {
    QTime midnight(0,0,0);
    qsrand(midnight.secsTo(QTime::currentTime()));
    return QColor( (rand() % 255), (rand() % 255), (rand() % 255) ).rgb();
}

// ----
bool parseXML( QDomDocument& doc, QFile & file ) {
//    QTextStream in(&file);
//    QString content = in.readAll(), err_msg;
    QString err_msg;
    int err_line = -1, err_col = -1;
    bool result = doc.setContent( &file, true, &err_msg, &err_line, &err_col );

    return error_handle( !result ,
                         file.fileName() + " : "
                         + err_msg + QString(" at line: %1, ").arg(err_line)
                         + QString("column: %1 ").arg(err_col) );


}


QImage  cvMatToQImage( const cv::Mat &inMat ) {
    switch ( inMat.type() )
    {
      // 8-bit, 4 channel
        case CV_8UC4:
        {
            QImage image( inMat.data, inMat.cols, inMat.rows, inMat.step, QImage::Format_RGB32 );

            return image;
        }

        // 8-bit, 3 channel
        case CV_8UC3:
        {
            QImage image( inMat.data, inMat.cols, inMat.rows, inMat.step, QImage::Format_RGB888 );

            return image.rgbSwapped();
        }

        // 8-bit, 1 channel
        case CV_8UC1:
        {
            static QVector<QRgb>  sColorTable;

            // only create our color table once
            if ( sColorTable.isEmpty() )
            {
            for ( int i = 0; i < 256; ++i )
                sColorTable.push_back( qRgb( i, i, i ) );
            }

            QImage image( inMat.data, inMat.cols, inMat.rows, inMat.step, QImage::Format_Indexed8 );

            image.setColorTable( sColorTable );

            return image;
        }
        default:
            qWarning() << "ASM::cvMatToQImage() - cv::Mat image type not handled in switch:" << inMat.type();
            break;
    }
}

QPixmap cvMatToQPixmap( const cv::Mat &inMat ) {
   return QPixmap::fromImage( cvMatToQImage( inMat ) );
}

cv::Mat QImageToCvMat( const QImage &inImage, bool inCloneImageData ){
      switch ( inImage.format() )
      {
         // 8-bit, 4 channel
         case QImage::Format_RGB32:
         {
            cv::Mat  mat( inImage.height(), inImage.width(), CV_8UC4, const_cast<uchar*>(inImage.bits()), inImage.bytesPerLine() );

            return (inCloneImageData ? mat.clone() : mat);
         }

         // 8-bit, 3 channel
         case QImage::Format_RGB888:
         {
            if ( !inCloneImageData )
               qWarning() << "ASM::QImageToCvMat() - Conversion requires cloning since we use a temporary QImage";

            QImage   swapped = inImage.rgbSwapped();

            return cv::Mat( swapped.height(), swapped.width(), CV_8UC3, const_cast<uchar*>(swapped.bits()), swapped.bytesPerLine() ).clone();
         }

         // 8-bit, 1 channel
         case QImage::Format_Indexed8:
         {
            cv::Mat  mat( inImage.height(), inImage.width(), CV_8UC1, const_cast<uchar*>(inImage.bits()), inImage.bytesPerLine() );

            return (inCloneImageData ? mat.clone() : mat);
         }

         default:
            qWarning() << "ASM::QImageToCvMat() - QImage format not handled in switch:" << inImage.format();
            break;
      }

      return cv::Mat();
   }

   // If inPixmap exists for the lifetime of the resulting cv::Mat, pass false to inCloneImageData to share inPixmap's data
   // with the cv::Mat directly
   //    NOTE: Format_RGB888 is an exception since we need to use a local QImage and thus must clone the data regardless
cv::Mat QPixmapToCvMat( const QPixmap &inPixmap, bool inCloneImageData) {
      return QImageToCvMat( inPixmap.toImage(), inCloneImageData );
}

QString cvMatQStringRepresent( cv::Mat* mat ) {
    QString str;

    for ( int i = 0; i < mat->rows; i++ ) {

        for (int j = 0; j < mat->cols; j++) {
            str+= QString("%1 ").arg( mat->at<double>( i, j) );
        }
        str += "\n";
    }

    return str;
}

#include <QFile>
void logCvMat( cv::Mat* mat, QString file_name ) {
    QFile file( file_name );
    if ( ! file.open( QFile::WriteOnly ) ) qDebug() << "mat_log.txt doesn't create";
    QTextStream stream( &file );
    for ( int i = 0; i < mat->rows; i++ ) {
        stream << "\n";
        for ( int j = 0; j < mat->cols; j++ ) {
            stream << mat->at<int>( i, j );
        }
    }

    file.close();
}

#include "cluster.h"
QString clusterDataQStringRepresent( Cluster* cluster, uint index ) {
    if ( index > cluster->data()->size() ) return QString();

    uint i = ( ( *cluster )( index ) ).i,
         j = ( ( *cluster )( index ) ).j;
    QString str("%1 %2");
    str.arg( i ).arg( j );

        for ( uint k = 0; k < 3; k++ ) {
        str += QString(" %1").
                arg( byteToInt( cluster->getImage()->at<unsigned char>( i, j + k ) ) );
    }
}
