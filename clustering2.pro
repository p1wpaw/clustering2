#-------------------------------------------------
#
# Project created by QtCreator 2016-02-13T19:08:08
#
#-------------------------------------------------

QT       += core gui xml testlib

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

win32 {
    INCLUDEPATH += C:\lib\libqxt\include\QxtWidgets
    LIBS += -LC:\lib\libqxt\lib

    INCLUDEPATH += C:/Qt/opencv2.4.1/install/include
    LIBS += -LC:\Qt\opencv2.4.1\install\x86\mingw\lib

    LIBS += -lQxtCored \
            -lQxtWidgetsd

    LIBS += -lopencv_core2411d \
        -lopencv_highgui2411d
}

unix {
    system(cp -rv ./config $$BUILDDIR)
#    Release:DESTDIR = ../build-linux/release
#    Debug:DESTDIR = ../build-linux/debug
    INCLUDEPATH += /usr/include/QxtCore
    INCLUDEPATH += /usr/include/QxtGui
#    LIBS+=-L/usr/lib64

    LIBS += -lQxtCore \
            -lQxtGui

    LIBS += -lopencv_core \
            -lopencv_highgui \
            -lopencv_imgcodecs

}



#DESTDIR = $$DESTDIR
#OBJECTS_DIR = $$DESTDIR/.obj
#MOC_DIR = $$DESTDIR/.moc
#RCC_DIR = $$DESTDIR/.rcc
#UI_DIR = $$DESTDIR/.ui


CONFIG += qxt
QXT += core gui

QMAKE_CXXFLAGS += -std=c++11

TARGET = clustering2
TEMPLATE = app


SOURCES += main.cpp\
    painter_contexts/paintercontroller.cpp \
    painter_contexts/framepaintercontext.cpp \
    painter_contexts/painter.cpp \
    painter_contexts/selectionmanager.cpp \
    algorithms/algorithmdialog.cpp \
    algorithms/tablealgorithmmodels.cpp \
    algorithms/algorithmdialogcontroller.cpp \
    algorithms/segmentationdialog.cpp \
    doubleslider.cpp \
    mainwindow.cpp \
    project.cpp \
    frame.cpp \
    cluster.cpp \
    projectmanager.cpp \
    distancefunctons.cpp \
    errorhandler.cpp \
    coreutils.cpp \
    projectdialog.cpp \
    modelstorage.cpp \
    algorithms/k_means.cpp \
    algorithms/serial_scan.cpp \
    algorithms/algorithmio.cpp \
    algorithms/serial_scan_io.cpp \
    data.cpp \
    painter_contexts/clusterpaitercontext.cpp \
    memtest.cpp \
    cluster_tree/clustertreewidget.cpp \
    cluster_tree/clustertreemodel.cpp \
    offsetoperator.cpp \
    clusterstatistics.cpp
    # staticpointerstorage.cpp

HEADERS  += mainwindow.h \
    project.h \
    frame.h \
    core.h \
    projectmanager.h \
    distancefunctons.h \
    errorhandler.h \
    projectdialog.h \
    data.h \
    painter_contexts/paintercontroller.h \
    painter_contexts/painter.h \
    painter_contexts/paintercontext.h \
    painter_contexts/selectionmanager.h \
    algorithms/algorithmdialog.h \
    algorithms/tablealgorithmmodels.h \
    algorithms/algorithmdialogcontroller.h \
    algorithms/segmentationdialog.h \
    doubleslider.h \
    modelstorage.h \
    algorithms/algorithms.h \
    algorithms/algorithmio.h \
    algorithms/patt_recog_alg_io.h \
    memtest.h \
    cluster_tree/clustertreewidget.h \
    cluster.h \
    cluster_tree/clustertreemodel.h \ #\
    cvmatindex.h \
    clusterstatistics.h
    # staticpointerstorage.h

FORMS    += mainwindow.ui \
    projectdialog.ui \
    algorithms/algorithmdialog.ui \
    algorithms/segmentationdialog.ui \
    doubleslider.ui \
    cluster_tree/clustertreewidget.ui

# SUBDIRS += painter_contexts

OTHER_FILES += \
    ../clustering2-build-desktop-Qt_4_8_6__4_8_6_________/debug/config/errors.dict \
    ../clustering2-build-desktop-Qt_4_8_6__4_8_6_________/debug/config/config.xml

RESOURCES += \
    resources.qrc
