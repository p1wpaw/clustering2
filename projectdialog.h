#ifndef PROJECTDIALOG_H
#define PROJECTDIALOG_H

#include <QDialog>
#include "project.h"

namespace Ui {
class ProjectDialog;
}

class ProjectDialog : public QDialog
{
    Q_OBJECT
public:
//    explicit ProjectDialog( QWidget *parent = 0 );
    explicit ProjectDialog( QWidget *parent = 0, Project * project = 0, const QString & name = QString() );
    ~ProjectDialog();

    void setProject( Project * project ) { project_ = project_; }

public slots:
    void    browseImage();
    void    adjustProject();

    QString getProjectName();
    QString getImageName();
    
    void setProjectName( QString& name );
    void setImageName( QString& name );

private:
    Project * project_;
    Ui::ProjectDialog *ui;
};

#endif // PROJECTDIALOG_H
