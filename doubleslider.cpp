#include "doubleslider.h"
#include "ui_doubleslider.h"

DoubleSlider::DoubleSlider(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DoubleSlider)
{
    ui->setupUi(this);
    connect( ui->slider_, SIGNAL( lowerValueChanged(int) ), SLOT( setLineEditLowerValue( int ) ) );
    connect( ui->slider_, SIGNAL( upperValueChanged(int) ), SLOT( setLineEditUpperValue( int ) ) );

    connect( ui->line_edit_lower_, SIGNAL( editingFinished() ), SLOT( setMinimum() ) );
    connect( ui->line_edit_upper_, SIGNAL( editingFinished() ), SLOT( setMaximum() ) );

    setLineEditLowerValue( ui->slider_->minimum() );
    setLineEditUpperValue( ui->slider_->maximum() );

    ui->slider_->setLowerValue( ui->slider_->minimum() );
    ui->slider_->setUpperValue( ui->slider_->maximum() );

//    connect( ui->slider_, SIGNAL(rangeChanged(int,int)), SLOT(setRange(int, int)) )
}

DoubleSlider::~DoubleSlider()
{
    delete ui;
}

QxtSpanSlider * DoubleSlider::spanSlider() {
    return ui->slider_;
}

void DoubleSlider::setLineEditLowerValue( int value ) {
    ui->line_edit_lower_->setText( QString("%1").arg( value ) );
}

void DoubleSlider::setLineEditUpperValue( int value ) {
    ui->line_edit_upper_->setText( QString("%1").arg( value ) );
}

void DoubleSlider::setMaximum() {
    int val;
    bool ok = false;
    val = ui->line_edit_upper_->text().toInt(&ok);

    if (!ok) {
        ui->line_edit_upper_->setText( QString("%1").arg( ui->slider_->maximum() ) );
        return;
    }
    ui->slider_->setMaximum( val );
}

void DoubleSlider::setMinimum() {
    int val;
    bool ok = false;
    val = ui->line_edit_lower_->text().toInt(&ok);

    if (!ok) {
        ui->line_edit_lower_->setText( QString("%1").arg( ui->slider_->minimum() ) );
        return;
    }
    ui->slider_->setMinimum( val );
}

void DoubleSlider::setMaximum( int max ) {
    int val;
    val = ui->slider_->upperValue();
    val = ( val > max ) ? max : val; // minimum

//    it's code string don't need, because signal (upper|lower)ValueChanged connected to custom slot set(lower|upper)Value()
//    see lines(9,10) and (26,33)
    setLineEditUpperValue( val );
    ui->slider_->setUpperValue( val );
    ui->slider_->setMaximum( max );
}

void DoubleSlider::setMinimum( int min ) {
    int val;
    val = ui->slider_->lowerValue();
    val = ( val < min ) ? min : val;

    setLineEditLowerValue( val );
    ui->slider_->setLowerValue( val );
    ui->slider_->setMinimum( val );
}
