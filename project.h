#ifndef PROJECT_H
#define PROJECT_H

#include <QDomElement>
#include "core.h"
#include "distancefunctons.h"
#include "modelstorage.h"
#include "algorithms/algorithmio.h"

class Project {
private:
    QString         name_;
    QString         file_name_;

    QString         image_file_name_;
    uint            tab_widget_index_;
    ProjectModelStorage*   model_storage_;

public:
    Project( const QString& name, const QString& abs_file_name );
    Project( const QDomElement& project_DOM, const QString& abs_file_name );
    Project();
    ~Project();

    QString &   getName() { return name_; }
    void        setName( const QString& name ) { name_ = name; }

    QString &   getFileName() { return file_name_; }
    void        setFileName( const QString& file_name ) { file_name_ = file_name; }

    QString &   getImageFileName() { return image_file_name_; }
    void        setImageFileName( const QString& image_file_name ) { image_file_name_ = image_file_name; }

    ProjectModelStorage * modelStorage() { return model_storage_; }

    void loadImage();
    void loadImage( const QString& file_name );
    void closeImage();
};



#endif // PROJECT_H
