#ifndef CLUSTER_H
#define CLUSTER_H

#include <QString>
#include <QPoint>
#include <list>
#include "data.h"
#include "core.h"
#include "clusterstatistics.h"

class Cluster : public Data, public ImageClusterStatistics { // double - representation type of data in selection (cv::Mat)
public:
    typedef std::vector<DataIndex>      DataIndexVec;
    typedef std::vector<Cluster*>       ClusterPtrVec;
    typedef std::list<Cluster*>         ClusterPtrList;

private:
    // extarmal data
    Cluster*            parent_;
    Cluster*            average_;
    uint                dimention_;

//    cv::Mat*            labels_;
    OffsetOperator      offset_operator_;

    // proerties
    uint                label_;
    QRgb                color_;

    // stored data
    DataIndexVec        data_;
    ClusterPtrList      children_;
    /*cv::Mat             statistics_;*/ // a row-vector for storage cluster characteristics
        // statistics_ format:
        // geometric_centroid(2),color_centroid(3), brightness centroid(1), square, perimeter, compactness

public:
    Cluster( Cluster*       parent = 0,
             cv::Mat* const image = 0,
             cv::Mat* const labels = 0,
             int            label=0,
             const QPoint&  labels_offset  = QPoint( 0, 0 ) );

    Cluster( Cluster* parent = 0, cv::Mat* const image = 0, const QString& file_name_ = QString("") );
    Cluster( ClusterPtrList* children );
    ~Cluster();

    void initializeFromChildren( ClusterPtrList* children );

    // access to statistics characterisitic
    double operator []( uint index );
    cv::Mat* getStatistics() { return &statistics_; }

    // calculate statistics
//    void    calculateCentroid   ( Cluster* cluster, cv::Mat* statistics );
//    void    calculateSqare      ( Cluster* cluster, cv::Mat* statistics );
//    void    calculatePerimeter  ( Cluster* cluster, cv::Mat* statistics );
//    void    calculateCompactness( Cluster* cluster, cv::Mat* statistics );
//    void    calculateStatistics ( Cluster* cluster, cv::Mat* statistics );

    // access to data (points) of cliuster
    DataIndex operator ()( uint index );
    DataIndex operator ()( uint i, uint j );
    DataIndexVec*   data() { return &data_; }
    uint            dataSize() { return data_.size(); }

    DataIndex& appendData( const DataIndex& value );
    void removeFromData( uint index );
    void removeFromData( uint i, uint j );

    // tree structure methods
    Cluster&            appendChild( Cluster* );
    void                removeChild( uint index );
    Cluster*            childAt( uint index );

    ClusterPtrList*     children() { return &children_; }

    void        setParent( Cluster* parent ) { parent_ = parent; }
    Cluster*    getParent() { return parent_; }

    // set-theoretic operations ( result to `this` )
    static Cluster * difference( Cluster *a, Cluster *b );

    // database and filesytem input\output tools
    void readCluster( const QString& file_name );
    void saveCluster( const QString& file_name );

    // property (get|set)ters
    void    setColor( const QRgb& color )   { color_ = color; }
    QRgb&   getColor()                      { return color_; }
    void    randomizeColor() { color_ = randomRgb(); }

    OffsetOperator& offsetOperator() { return offset_operator_; }
};
typedef std::vector<Cluster*>   ClusterPtrList;
typedef std::set<Cluster*>      ClusterPtrSet;
//     // set-theoretic operations ( result to `new Cluster` )
Cluster * unite( Cluster *a,  Cluster *b );

#endif // CLUSTER_H
