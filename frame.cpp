#include "frame.h"

FrameSelection::FrameSelection( const QRect& area, QWidget * view, bool modifiable )

{
//    area_.push_back( QPoint( area.x(), area.y() ) );
//    area_.push_back( QPoint( area.x() + area.width(), area.y() ) );
//    area_.push_back( QPoint( area.x() + area.width(), area.y() + area.height() ) );
//    area_.push_back( QPoint( area.x(), area.y() + area.height() ) );

    area_ = area;
    modifiable_ = modifiable;
    type_ = FrameSelectionType;
}

//FrameSelection::FrameSelection( const QPointList& area, QWidget * view, bool modifiable, bool sorted ) {
//    area_ = area;
//    modifiable_ = modifiable;
//}

void    FrameSelection::resize( QRect new_size ) {}

//void    FrameSelection::add( const QRect& summand ) {}

//void    FrameSelection::add( const int w, const int h ) {

//}

void    FrameSelection::move( int dx, int dy ) {
    area_.setX( area_.x() + dx );
    area_.setY( area_.y() + dy );
}

void    FrameSelection::moveTo( int x, int y ) {
    area_.setX( x );
    area_.setY( y );
}

void FrameSelection::resize( int w, int h ) {
    area_.setWidth( w );
    area_.setHeight( h );
}
