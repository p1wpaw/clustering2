#include "projectdialog.h"
#include "ui_projectdialog.h"
#include <QFileDialog>

//ProjectDialog::ProjectDialog(QWidget *parent) :
//    QDialog(parent),
//    ui(new Ui::ProjectDialog)
//{
//    ui->setupUi( this );
//}

ProjectDialog::ProjectDialog( QWidget *parent, Project* project, const QString & name ) :
    QDialog(parent),
    ui(new Ui::ProjectDialog),
    project_( project )
{

    ui->setupUi( this );
    ui->lineEditName->setText( name );
    connect ( this, SIGNAL( accepted() ), SLOT( adjustProject() ) );
    connect ( this->ui->buttonBrowseImage, SIGNAL(clicked()), SLOT( browseImage() ) );
}

void ProjectDialog::adjustProject() {
    project_->setName( ui->lineEditName->text() );
    project_->loadImage( ui->lineEditImageFileName->text() );
}

void ProjectDialog::browseImage() {
    QString name = QFileDialog::getOpenFileName(
                                0,
                                "Select Image",
                                QApplication::applicationDirPath(),
                                "Images (*.png *.bmp *.jpg);;All files (.*)");
    ui->lineEditImageFileName->setText( name );
}

QString ProjectDialog::getProjectName() {
    return ui->lineEditName->text();
}
QString ProjectDialog::getImageName() {
    return ui->lineEditImageFileName->text();
}

void ProjectDialog::setProjectName( QString& name ) {
    return ui->lineEditName->setText( name );
}
void ProjectDialog::setImageName( QString& name ) {
    return ui->lineEditImageFileName->setText(  name );
}

ProjectDialog::~ProjectDialog()
{
    delete ui;
}
