#include "painter.h"
#include "project.h"
#include "core.h"
#include "frame.h"

//Painter::Painter(QWidget *parent) :
//    QWidget(parent)
//{
//    update();
//}

Painter::Painter(QWidget *parent, cv::Mat * const image ) : QWidget(parent)
  , image_( image )
{
    q_image_ = cvMatToQImage( *image_ );
    pix_map_ = QPixmap::fromImage( q_image_ );

    update();
}

Painter::~Painter() {
}

void Painter::displayImage( cv::Mat *image ) {}

void Painter::scaleImage( QRect& scale ) {}

PainterContext* Painter::getMouseObserver() {
    ContextMap::iterator it = contexts_.begin();
    for ( it; it != contexts_.end(); ++it ) {
        if  ( it->second->getMouseObserveCondition() )
            return ( *it ).second;
    }
    return 0;
}

void Painter::setMouseObserver( PainterContext * context ) {
    ContextMap::iterator it = contexts_.find( context->layer() );
    if ( it == contexts_.end() ) return;
    PainterContext * current = getMouseObserver();
    if ( current ) current->setMouseObserveCondition( false );
    context->setMouseObserveCondition( true );
}

void Painter::mousePressEvent( QMouseEvent *event ) {
    if ( event->button() == Qt::LeftButton ) mouse_button_press_ = -1;
    else if ( event->button() == Qt::RightButton ) mouse_button_press_ =  1;
    else mouse_button_press_ = 0;

    FrameSelectionPainterContext * mouse_observer;
    mouse_observer = static_cast<FrameSelectionPainterContext*>( getMouseObserver() );
    if ( !mouse_observer ) return;

    mouse_observer->mouseEvent( event );
    update();
}

void Painter::mouseMoveEvent( QMouseEvent * event ) {
    FrameSelectionPainterContext * mouse_observer;
    mouse_observer = static_cast<FrameSelectionPainterContext*>( getMouseObserver() );
    if ( !mouse_observer ) return;

    if ( mouse_button_press_ == -1 ) {
        mouse_observer->mouseEvent( event );
        update();
    }
}

void Painter::wheelEvent( QWheelEvent * event ) {}

void Painter::mouseReleaseEvent( QMouseEvent * event ) {
    mouse_button_press_ = 0;
}


void    Painter::paintEvent( QPaintEvent * event ) {
//    QPixmap p_map( image_->cols, image_->rows );
    QPainter p1( this ), p2( &pix_map_ );
    ContextMap::iterator it = contexts_.begin();

    p1.drawPixmap( 0, 0, pix_map_ );
    for ( it; it != contexts_.end(); ++it ) {
        it->second->draw( &p1 );
    }

    QWidget::paintEvent( event );
}



