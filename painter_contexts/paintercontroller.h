#ifndef PAINTERCONTROLLER_H
#define PAINTERCONTROLLER_H

#include <QObject>
#include <QToolBar>
#include "painter.h"
#include "painter_contexts/selectionmanager.h"
#include "modelstorage.h"

// class PainterController - provides Control methods and

class PainterController : public QObject
{
    Q_OBJECT
private:
    Painter*        painter_;
    QToolBar*       tool_bar_;
    Project*        current_project_;

public:
    explicit PainterController( QObject *tool_bar, Project* project );

    Painter * getPainter()  { return painter_; }
    void setPainter( Painter * painter ) { painter_ = painter; }

    QToolBar* getToolBar() { return tool_bar_; }
    void setToolBar( QToolBar* tool_bar ) { tool_bar_ = tool_bar; }

    Project * getCurrentProject() { return current_project_; }
    void setCurrentProject( Project * project );
    ContextMap& getContexts () const { return painter_->getContexts(); }

    bool event( QEvent * e );

signals:
    
public slots:
    void setSqareSelectionContext();
    void setArbitrarySelectionContext();
    void setScaleContext();
    void setClusterContext();
    void setClusterContext( Cluster* cluster );
};

#endif // PAINTERCONTROLLER_H
