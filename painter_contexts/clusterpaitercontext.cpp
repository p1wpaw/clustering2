#include "paintercontext.h"
#include "cluster.h"
#include <limits>

ClusterPainterContext::ClusterPainterContext( PainterContext* source )
    : PainterContext( source ) {}

ClusterPainterContext::ClusterPainterContext( Cluster* cluster, bool draw_children )
    : PainterContext( false ), draw_children_( draw_children ), cluster_( cluster )  {

}

ClusterPainterContext::~ClusterPainterContext() {

}

void ClusterPainterContext::draw( QPainter * const painter ) {
    drawCluster( cluster_, painter );
}

void ClusterPainterContext::clear( bool delete_values ) {}

void ClusterPainterContext::mouseEvent( QMouseEvent *event ) {}

void ClusterPainterContext::drawCluster( Cluster* cluster, QPainter * const painter ) {
    if ( !cluster_ ) return;
    drawClusterData( cluster, painter );
    if ( !draw_children_ ) return;

//    Cluster::ClusterPtrVec* children = cluster->children();
//    int size = children->size();
//    for ( uint i = 0; i < size; i++ )
//        if ( ( *children )[i] )
//            drawCluster( ( *children )[i], painter );
    Cluster::ClusterPtrList::iterator it = cluster->children()->begin();
    for ( it; it != cluster->children()->end(); ++it ) {
        drawCluster( *it, painter );
    }

}

void ClusterPainterContext::drawClusterData( Cluster* cluster, QPainter * const painter ) {
    if ( !cluster ) return;
    QRgb    rgb = cluster->getColor();

    setQPainterPenColor( painter, rgb );
    uint size = cluster->dataSize();

    for ( uint index = 0; index < size; index++ ) {
        int     i = ( *cluster )( index ).i,
                j = ( *cluster )( index ).j;
        painter->drawPoint( i, j );
    }
    rgb ^= std::numeric_limits<uint>::max();
    setQPainterPenColor( painter, rgb );

    uint ci = ( uint )( ( *cluster )[0] ),
         cj = ( uint )( ( *cluster )[1] ) ;
    painter->drawPoint( ci, cj );
    painter->drawEllipse( ci, cj, 5, 5 );
}
