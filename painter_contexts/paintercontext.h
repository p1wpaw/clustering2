#ifndef PAINTERCONTEXT_H
#define PAINTERCONTEXT_H

#include <QPainter>
#include <QMouseEvent>
#include <set>
#include "frame.h"
#include "cluster.h"
#include "core.h"

class PainterContext
{
protected:
    bool enabled_;
    bool mouse_observe_;
public:
    enum Layer {
        NotDrawableLayer,
        ImageLayer,
        ClusterLayer,
        FramesLayer,
    };

protected:
    static const Layer layer_ = NotDrawableLayer;

public:

    PainterContext( bool mouse_observe ) :
        mouse_observe_(mouse_observe) {}

    PainterContext( PainterContext* source = 0 )
        : mouse_observe_( source->getMouseObserveCondition() ) {}

    virtual ~PainterContext() {}

    bool operator < ( PainterContext* other )
        { return layer() <  other->layer(); }

    bool operator > ( PainterContext* other )
        { return layer() >  other->layer(); }

    bool operator == ( PainterContext* other )
        { return layer() == other->layer(); }

    virtual const Layer layer() { return layer_; }

    virtual void draw( QPainter* const  painter ) = 0;
    virtual void clear( bool delete_values = false ) = 0;

    virtual void mouseEvent( QMouseEvent * event ) = 0;
    virtual bool getMouseObserveCondition() { return mouse_observe_; }
    virtual void setMouseObserveCondition( bool condition ) { mouse_observe_ = condition; }
};

class ClusterPainterContext : public PainterContext {
private:
    static const Layer layer_ = PainterContext::ClusterLayer;
    Cluster* cluster_;
    bool draw_children_;

public:
    ClusterPainterContext( PainterContext* source = 0 );
    ClusterPainterContext( Cluster* cluster = 0, bool draw_children = true );
//    ClusterPainterContext( bool mouse_observe ) : PainterContext(mouse_observe){}
    ~ClusterPainterContext();

//    ClusterPainterContext() : layer_( 3 ) {}
//    void append ( ContextRepresented* cluster );
//    void remove ( ContextRepresented* cluster );

    void draw( QPainter* const  painter );
    void clear( bool delete_values = false  );

    void    drawClusterData( Cluster* cluster = 0, QPainter * const painter = 0 );
    void    drawCluster( Cluster* cluster = 0, QPainter * const painter = 0 );

    virtual const Layer layer() { return layer_; }

    Cluster* getCluster () { return cluster_;  }
    void setCluster( Cluster* const cluster ) { cluster_ = cluster; }

    virtual void mouseEvent( QMouseEvent *event );
    virtual bool getMouseObserveCondition() { return mouse_observe_; }
    virtual void setMouseObserveCondition( bool condition ) { mouse_observe_ = condition; }

};

class FrameSelectionPainterContext : public PainterContext {
private:
    FrameSelection * mouse_observed_frame;
    static const Layer layer_ = PainterContext::FramesLayer;
    SelectionPtrSet* frames_;

public:
//    FrameSelectionPainterContext() : layer_( 4 ) {}
    FrameSelectionPainterContext( PainterContext* source = 0 );
    FrameSelectionPainterContext( bool mouse_observe );
    FrameSelectionPainterContext( SelectionPtrSet* frames, bool mouse_observe );
    ~FrameSelectionPainterContext();

    virtual void draw( QPainter*  painter );
    virtual void clear( bool free_memory = false );

    virtual const Layer layer() { return layer_; }

    SelectionPtrSet* getFrames () { return frames_;  }
    void setFrames( SelectionPtrSet* frames ) { frames_ = frames ;  }

    void mouseEvent( QMouseEvent * event );
    virtual bool getMouseObserveCondition() { return mouse_observe_; }
    virtual void setMouseObserveCondition( bool condition ) { mouse_observe_ = condition; }

};

#endif // PAINTERCONTEXT_H
