#include "paintercontext.h"

//bool PainterContext::cmpContextPriorityGreater( PainterContext* a, PainterContext* b ) {
//    return a.priority() > b.priority();
//}
FrameSelectionPainterContext::FrameSelectionPainterContext( PainterContext* source )
    : PainterContext( source ) {}

FrameSelectionPainterContext::FrameSelectionPainterContext( bool mouse_observe )
    : PainterContext( mouse_observe ) {}

FrameSelectionPainterContext::FrameSelectionPainterContext( SelectionPtrSet* frames, bool mouse_observe )
    : PainterContext( mouse_observe ), frames_( frames ) {}

FrameSelectionPainterContext::~FrameSelectionPainterContext() {
    SelectionPtrSet::iterator it = frames_->begin();

    for ( it; it != frames_->end(); ++it )
        delete (*it);
    frames_->clear();
}

void FrameSelectionPainterContext::draw(QPainter * painter) {
    SelectionPtrSet::iterator it = frames_->begin();
    painter->setPen( QPen( Qt::black, 1.2, Qt::DotLine ) );
    for ( it; it != frames_->end(); ++it ) {
        if ( (*it)->type() == Selection::FrameSelectionType ) {
            FrameSelection* frame = static_cast<FrameSelection*>( *it );
            painter->drawRect( *frame );
        }
    }
}

void FrameSelectionPainterContext::clear( bool free_memory ) {
    if ( free_memory ) {
        SelectionPtrSet::iterator it = frames_->begin();
        for ( it; it != frames_->end(); ++it )
            delete (*it);
    }
    frames_->clear();
}

void FrameSelectionPainterContext::mouseEvent( QMouseEvent* event ) {
    if ( event->type() == QEvent::MouseButtonPress )
        if ( event->button() == Qt::LeftButton ) {
            mouse_observed_frame = new  FrameSelection(
                                          QRect( event->x(), event->y(), 0, 0 )
                                        );
            frames_->insert( mouse_observed_frame ); // ModelStorage Frames
        }
    if ( event->type() == QEvent::MouseMove ) {
        mouse_observed_frame->resize( event->x() - mouse_observed_frame->getArea().x(),
                                      event->y() - mouse_observed_frame->getArea().y() );
    }
    if ( event->type() == QEvent::MouseButtonRelease )
        mouse_observed_frame = 0;
}
