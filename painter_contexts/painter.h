#ifndef PAINTER_H
#define PAINTER_H


#include <QWidget>
#include <QPainter>
#include <QPen>
#include <QPaintEvent>
#include <QColor>
#include <unordered_map>
#include "cluster.h"
#include "painter_contexts/paintercontext.h"
//#include "painter_contexts/contextmanager.h"

//    enum SelectMode {
//        NoSelect, RectangularRegion, ArbitraryRegion, PolygonalRegion
//    };
//    uint select_mode_;

//    enum DisplayMode {
//        ImageMode, ClusterMode
//    }; (1)

//typedef std::set<PainterContext*, bool(*)(const PainterContext*, const PainterContext*)> ContextMap;

typedef std::pair< PainterContext::Layer, PainterContext* > ContextPair;
typedef std::multimap< PainterContext::Layer, PainterContext* > ContextMap;

class Project;

class Painter : public QWidget
{
    Q_OBJECT
public:
    Painter(QWidget *parent = 0, cv::Mat * const cv_image = 0 );
    ~Painter();

private:
    cv::Mat *       image_;
    QImage          q_image_;
    QPixmap         pix_map_;
    QList<QRect>    scale_pos_stack_;  // initialize this list as one-element contained
    // this (first) element are (0,0,img_width, img_height)
    // this abstraction based on Decart's coordinate system:
    char            mouse_button_press_;  // -1 left, 0 - not pressed,  1 - right
    char            mouse_wheel_scroll_;  // -1 down, 0 - not scrolled, 1 - up
    ContextMap      contexts_;
    uint            display_mode_;
    Project*        project_;

signals:

public slots:
    void        displayImage( cv::Mat * image = 0 );
    void        scaleImage( QRect& );

public:
    void        setProject( Project* project ) { project_ = project; }
    Project*    getProject() { return project_;  }

    cv::Mat * const     getCvImage()  { return image_; }
    void                setCvImage( cv::Mat * const image ) { image_ = image; }

    QImage&     getQImage() { return q_image_; }
    void        setQImage( const QImage & q_image ) { q_image_ = q_image; }

    PainterContext*     getMouseObserver();
    void                setMouseObserver( PainterContext * context );

    void    wheelEvent( QWheelEvent * );
    void    mousePressEvent(QMouseEvent *);
    void    mouseReleaseEvent( QMouseEvent * );
    void    mouseMoveEvent( QMouseEvent * );
    void    paintEvent( QPaintEvent * );

    ContextMap& getContexts() { return contexts_;  }
    void    setContexts( ContextMap& contexts ) { contexts_ = contexts; }
};

#endif // PAINTER_H
