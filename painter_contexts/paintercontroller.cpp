#include "paintercontroller.h"
#include <QAction>
#include <QIcon>
#include <QMessageBox>
#include "errorhandler.h"
#include "project.h"
#include "modelstorage.h"
#include "frame.h"

PainterController::PainterController( QObject *tool_bar, Project * project ) :
    QObject(tool_bar),
    painter_            ( 0 ),
    tool_bar_           ( qobject_cast<QToolBar*>( tool_bar ) ),
    current_project_    ( project )
{
    QAction*  sqare_selection_action =  new QAction(QIcon(":/icons/sel_frame.ico"), tr("��������� ������"), tool_bar_ );
    tool_bar_->addAction( sqare_selection_action );
    connect(  sqare_selection_action, SIGNAL( triggered() ), SLOT( setSqareSelectionContext() ) );

    QAction*  arbitrary_scale_action  =  new QAction(QIcon(":/icons/sel_arbitrary.png"), tr("������������ ���������"), tool_bar_ );
    tool_bar_->addAction( arbitrary_scale_action );
    connect( arbitrary_scale_action, SIGNAL( triggered() ), SLOT( setArbitrarySelectionContext()
                                                                  ) );

    QAction*  scale_action  =  new QAction(QIcon(":/icons/zoom.png"), tr("������"), tool_bar_ );
    tool_bar_->addAction( scale_action );
    connect( scale_action, SIGNAL( triggered() ), SLOT( setScaleContext() ) );

    QAction*  cluster_show_action = new QAction(QIcon(":/icons/clusters.png"), tr("������������"), tool_bar_ );
    tool_bar_->addAction( cluster_show_action );
    connect( cluster_show_action, SIGNAL( triggered() ), SLOT( setClusterContext() ) );

    tool_bar_->addSeparator();
}

void PainterController::setSqareSelectionContext() {
    if ( error_handle( !painter_, err_text("ERR_NO_PAINTER") ) ) return;
    FrameSelectionPainterContext * frame_context = 0;
    ContextMap::iterator it = getContexts().find( PainterContext::FramesLayer );

    if ( it == getContexts().end() ) {
        SelectionPtrSet* frames = current_project_->modelStorage()->getSelectionPtrSet();
        frame_context = new FrameSelectionPainterContext(
                                frames,
                                true );

        getContexts().insert( ContextPair(
                                  PainterContext::FramesLayer,
                                  frame_context ) );

        painter_->setMouseObserver( frame_context );
    }
}

void PainterController::setCurrentProject( Project *project ){
    current_project_ = project;
}

void PainterController::setArbitrarySelectionContext() {
    QMessageBox::information( 0, tr("arbitrary"), tr("select") );
}

void PainterController::setScaleContext() {
    QMessageBox::information( 0, tr("scale"), tr("scale") );
}

void PainterController::setClusterContext() {
    setClusterContext( 0 );
}

void PainterController::setClusterContext( Cluster* cluster ) {
    if ( error_handle( !painter_, err_text("ERR_NO_PAINTER") ) ) return;
    if ( !cluster ) return;

    ClusterPainterContext * cluster_context = 0;
    ContextMap::iterator it = getContexts().find( PainterContext::ClusterLayer );

    if ( it == getContexts().end() ) {
        cluster_context = new ClusterPainterContext( cluster );

        getContexts().insert( ContextPair(
                                  PainterContext::ClusterLayer,
                                  cluster_context ) );
    } else {
        static_cast<ClusterPainterContext*>( it->second )->setCluster( cluster );
    }
    painter_->update();
}

bool PainterController::event( QEvent *e ) {
//    if ( e->type() == ALL_ALG_COMPLETE_EV ) {
//        AllAlgorithmsCompleteEvent*
//            aall_event = static_cast<AllAlgorithmsCompleteEvent*>( e );

//        ClusterPainterContext*
//            cluster_context = static_cast<ClusterPainterContext*> (
//                        *( getContexts().find( PainterContext::ClusterLayer ).second ) );

//        AlgorithmIOController* alg_io_controller =
//                aall_event->getAlgorithmIOContoller();

//        cluster_context->setCluster( *( alg_io_controller->ioMap()->begin() ) );
//    }
    return QObject::event( e );
}
