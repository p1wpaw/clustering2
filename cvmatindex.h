#ifndef CVMATINDEX_H
#define CVMATINDEX_H

#include <opencv2/core/core.hpp>

// raw read/write cv::Mat elenment interface
class CvMatIndex {
private:
    cv::Mat* mat_;

public:
    CvMatIndex( cv::Mat * mat  ) : mat_( mat ) {}

    cv::Mat*    getMat() { return mat_; }
    cv::Mat&    getMatRef() { return *mat_; }
    void        setMat( cv::Mat* mat ) { mat_ = mat; }

    // See http://www.convertdatatypes.com/ for valid cast

    // this method is UNSAFE, check diapasones and `sizeof(Type)` before it used
    template <typename Type> Type& at( int i, int j ) {
        return (Type&) ( mat_->data[ mat_->step[0] * i + mat_->step[1] * j ] );
    }
};

#endif // CVMATINDEX_H
