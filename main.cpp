#include <QApplication>
#include <QTextCodec>
#include "mainwindow.h"
#include "errorhandler.h"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "painter_contexts/painter.h"

void test() {
    cv::Mat mat = cv::imread(
        std::string(
            "C:/Files/qt/clustering_win/clustering2/resources/images/5.jpg"
        ),
        CV_LOAD_IMAGE_COLOR
    );
    cv::namedWindow( "Display window", CV_WINDOW_AUTOSIZE );// Create a window for display.
    cv::imshow( "Display window", mat );                   // Show our image inside it.

    cv::waitKey(0);
}

int main(int argc, char *argv[])
{
//    int alg_complete_ev_id      = QEvent::registerEventType( ALG_COMPLETE_EV ),
//        all_alg_complete_ev_id  = QEvent::registerEventType( ALL_ALG_COMPLETE_EV );

//    qDebug() << alg_complete_ev_id << all_alg_complete_ev_id;

    QTextCodec *codec = QTextCodec::codecForName("CP-1251");
    QTextCodec::setCodecForLocale(codec);
    QTextCodec::setCodecForCStrings ( codec );
    QTextCodec::setCodecForTr(codec); // error

    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    
    return a.exec();
}
