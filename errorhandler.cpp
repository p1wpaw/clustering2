#include "errorhandler.h"
#include <QStringList>

std::map<QString, QString> ErrorHandler::errors_;

QString ErrorHandler::getErrorText(const QString& key, const QString& extra ) {
    return errors_[key] + extra;
}

QString ErrorHandler::getErrorText(const QString& key) {
    return errors_[key];
}

#define NOT !
bool error_handle( bool error_condition, const QString &text, QString * buffer ) {  // example usage: bool has_error = error_handle( file.open(), err_msg[, buffer] );
    QMessageBox msg_box;

    if ( error_condition ) {
        if ( ! buffer ) {
            msg_box.information(0, "������", text );
        } else
            buffer->append( text );
    }
    return (error_condition);
}
#undef NOT

void ErrorHandler::initialize() {
#ifdef __linux__
    QFile file( QApplication::applicationDirPath() + "/config/errors.dict" );
#else
    QFile file( QApplication::applicationDirPath() + "\\config\\errors.dict" );
#endif
    QMessageBox msg_box;
    QByteArray line;
    QString str_line;
    QStringList kv;
    QRegExp regex;

    if ( ! file.open(QIODevice::ReadOnly) ) {
        msg_box.information(0, "Critical Error", "���������� ������� ���� �������� ������: config\\errors.dict");
        exit(1);
    }

    while ( !file.atEnd() ) {
        line = file.readLine( 2048 );
        str_line = line;
        if (str_line[0] == QChar('#')) continue;

        kv = str_line.split( QRegExp(" {0,}= {0,}") );
        if( ( kv.length() != 2 ) ) continue;
        errors_.insert(std::pair<QString, QString>( kv[0], kv[1] ));
        if ( ! line.length() ) break;
    }
    file.close();
}
//___________________________________________________________________________________________________________________
