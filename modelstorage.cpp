#include "modelstorage.h"

ProjectModelStorage::ProjectModelStorage( Project* project ) : project_(project) {}
ProjectModelStorage::~ProjectModelStorage() {
    clearFrames();
}

void ProjectModelStorage::removeFrame( FrameSelection* frame ) {}
void ProjectModelStorage::clearFrames() {
    SelectionPtrSet::iterator it = frames_.begin();

    for ( it; it != frames_.end(); ++it ) delete *it;
}

//void ProjectModelStorage::insertCluster( const QRect& rect ) {}
//void ProjectModelStorage::removeCluster( FrameSelection* frame ) {}
//void ProjectModelStorage::clearClusters() {}
