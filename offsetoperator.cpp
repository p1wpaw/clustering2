#include "data.h"

Data::DataIndex operator + ( Data::DataIndex& a, Data::DataIndex& b ) {

}
Data::DataIndex operator - ( Data::DataIndex& a, Data::DataIndex& b ) {

}

OffsetOperator::OffsetOperator( cv::Mat* labels,
                                const QPoint& offset )
: labels_( labels ) {
    offset_.i = offset.y();
    offset_.j = offset.x();
}

OffsetOperator::OffsetOperator( cv::Mat* labels,
                                const Data::DataIndex& offset  )
: labels_( labels ), offset_( offset ) {

}

// two below methods returns offset of index value ( index -> offset )
Data::DataIndex OffsetOperator::offsetFromIndex( int i, int j ) {
    return Data::DataIndex({ ( i - offset_.i ) ,
                             ( j - offset_.j ) });
}
Data::DataIndex OffsetOperator::offsetFromIndex( const Data::DataIndex& index ) {
    return Data::DataIndex({ ( index.i - offset_.i ),
                             ( index.j - offset_.j) });
}

// two below ethods returns normal index of offset value ( offset -> index )
Data::DataIndex OffsetOperator::indexFromOffset( int i, int j ) {
    return Data::DataIndex({ ( i + offset_.i ),
                             ( j + offset_.j ) });
}
Data::DataIndex OffsetOperator::indexFromOffset( const Data::DataIndex& index ) {
    return Data::DataIndex({ ( index.i + offset_.i ),
                             ( index.j + offset_.j) });
}

QPoint  OffsetOperator::getOffsetAsQPoint() {
    return QPoint ( offset_.j, offset_.i );
}
