#ifndef CLUSTERSTATISTICS_H
#define CLUSTERSTATISTICS_H
#include <opencv2/core/core.hpp>

class Cluster;

// class provides base interfave for tree cluster structure of statistics, computing and storing it

class ClusterStatistics
{
public:
    typedef struct Count__ {
        int children;
        int data;

        Count__& operator +=( Count__ & other ) {
            children    += other.children;
            data        += other.data;
            return *this;
        }
    } Count; // structure for count of cluster elements
    // Alphabet order field names

protected:
    cv::Mat     statistics_;
    virtual void calculateStatistics( Cluster* cluster ) = 0;
    Count count_;

public:
    ClusterStatistics() : count_({ 0, 0 }) {}
    virtual ~ClusterStatistics() {}

    virtual Count   calculate( Cluster* cluster ) = 0;
    virtual cv::Mat* getStatistics() { return &statistics_; }
};

class ImageClusterStatistics : public ClusterStatistics {
public:
    enum Index {
        RowIndex,       // 0
        ColomnIndex,    // 1
        BlueIndex,      // 2
        GreenIndex,     // 3
        RedIndex,       // 4
        BrightnessIndex,// 5
//        DispersionIndex,// 6
        SquareIndex,    // 6
        PerimeterIndex, // 7
        CompactnessIndex,// 8
        Size // 9
    };
    ImageClusterStatistics() : ClusterStatistics() {};
    void calculateStatistics( Cluster* cluster );

private:
    Count   calculate( Cluster* cluster );
    void    calculateCentroid( Cluster *cluster );
    void    calculateSquare( Cluster* cluster );
    void    calculatePerimteter( Cluster *cluster );
    void    calculateCompactness( Cluster *cluster );
//    void    calculateDispersion( Cluster *cluster, cv::Mat* statistics );
};
#endif // CLUSTERSTATISTICS_H
