#ifndef STATICPOINTERSTORAGE_H
#define STATICPOINTERSTORAGE_H

class MainWindow;
class DoubleSlider;
class ClusterTreeWidget;
class AlgorithmDialog;
class SegmentationDialog;
class Painter;
class PainterController;
class ProjectManager;
class AlgorithmDialogController;

class StaticPointerStorage
{
private:
    static  MainWindow*                 main_window_;
    static  DoubleSlider*               double_slider_;
    static  ClusterTreeWidget*          cluster_tree_widget_;
    static  AlgorithmDialog*            algorithm_dialog_;
    static  SegmentationDialog*         segmentation_dialog_;
    static  Painter*                    painter_;
    static  PainterController*          painter_controller_;
    static  ProjectManager*             project_manager_;
    static  AlgorithmDialogController*  algorithm_dialog_controller_;

public:
// sed 's/\(static \)\(.*\)/\1 \2 \2/g' 1 | sed 's/\(static.* \)\([A-Z]*\)/\1 \L\2/g' | sed 's/\(static .*\* \) *\([a-z]*\)\([A-Z][a-z]*\)\**/\1 q \L\2_\3_/g'
//awk '/static/{print $2,$3}' 1 | sed 's/\(.*\)\*\(.*\)_;/void set\1( \1* \2 ) { \2_ = \2; }\n \1* get\1() { return \2_; }\n/g'
    static void initialize();

    static void setMainWindow( MainWindow*  main_window ) {  main_window_ =  main_window; }
    static MainWindow* getMainWindow() { return  main_window_; }

    static void setDoubleSlider( DoubleSlider*  double_slider ) {  double_slider_ =  double_slider; }
    static DoubleSlider* getDoubleSlider() { return  double_slider_; }

    static void setClusterTreeWidget( ClusterTreeWidget*  cluster_tree_widget ) {  cluster_tree_widget_ =  cluster_tree_widget; }
    static ClusterTreeWidget* getClusterTreeWidget() { return  cluster_tree_widget_; }

    static void setAlgorithmDialog( AlgorithmDialog*  algorithm_dialog ) {  algorithm_dialog_ =  algorithm_dialog; }
    static AlgorithmDialog* getAlgorithmDialog() { return  algorithm_dialog_; }

    static void setSegmentationDialog( SegmentationDialog*  segmentation_dialog ) {  segmentation_dialog_ =  segmentation_dialog; }
    static SegmentationDialog* getSegmentationDialog() { return  segmentation_dialog_; }

    static void setPainter( Painter*  painter ) {  painter_ =  painter; }
    static Painter* getPainter() { return  painter_; }

    static void setPainterController( PainterController*  painter_controller ) {  painter_controller_ =  painter_controller; }
    static PainterController* getPainterController() { return  painter_controller_; }

    static void setProjectManager( ProjectManager*  project_manager ) {  project_manager_ =  project_manager; }
    static ProjectManager* getProjectManager() { return  project_manager_; }

    static void setAlgorithmDialogController( AlgorithmDialogController*  algorithm_dialog_controller ) {  algorithm_dialog_controller_ =  algorithm_dialog_controller; }
    static AlgorithmDialogController* getAlgorithmDialogController() { return  algorithm_dialog_controller_; }

};

#endif // STATICPOINTERSTORAGE_H
