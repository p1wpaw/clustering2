#ifndef MODELSTORAGE_H
#define MODELSTORAGE_H

#include <opencv2/core/core.hpp>
#include "frame.h"
#include "cluster.h"
#include "algorithms/patt_recog_alg_io.h"
#include "cluster_tree/clustertreemodel.h"
#include "distancefunctons.h"

class Project;

class ProjectModelStorage
{
private:
    Project*            project_;
    SelectionPtrSet     frames_;
    cv::Mat             image_;
    DistanceFunc        distance_func_;
    AlgorithmIOController
                        algorithm_io_controller_;
    ClusterTreeModel    cluster_tree_;

public:
    ProjectModelStorage( Project* project = 0);

    ~ProjectModelStorage();
    Project* getProject() { return project_; }
    void     setProject( Project* project ) { project_ = project; }

    SelectionPtrSet*   getSelectionPtrSet() {
        return &frames_; }
    void        insertFrame( const QRect& rect );
    void        removeFrame( FrameSelection* frame );
    void        clearFrames();

//    ClusterPtrSet*  clusterSet() { return &clusters_; }
//    void            insertCluster( const QRect& rect );
//    void            removeCluster( FrameSelection* frame );
//    void            clearClusters();

    cv::Mat*    getCvImage() { return &image_; }
    void        setCvImage( const cv::Mat& image ) { image_ = image; }


    DistanceFunc     getDistanceFunction();
    void             setDistanceFunction( DistanceFunc distance_func );

    AlgorithmIOController* algorithmIOController() { return &algorithm_io_controller_; }

    ClusterTreeModel* clusterTree() { return &cluster_tree_; }
};



#endif // MODELSTORAGE_H
