#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFile>
#include <QMessageBox>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "errorhandler.h"
#include "projectmanager.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    project_manager_( 0 ),
    painter_controller_( 0 ),
    alg_dialog_controller_( 0 ),
    cluster_tree_widget_( 0 )

{
    ui->setupUi(this);
    ui->tabWidget->clear();

    ErrorHandler::initialize();
    uint conf = readConfig();

    project_manager_        = new ProjectManager( this );
    painter_controller_     = new PainterController( ui->mainToolBar, 0 );

    alg_dialog_controller_  = new AlgorithmDialogController( ui->mainToolBar );
    cluster_tree_widget_    = new ClusterTreeWidget( this, painter_controller_, alg_dialog_controller_ );
//    if ( conf ) exit( conf );
}

MainWindow::~MainWindow()
{
    if ( ui )                       delete ui;
    if ( project_manager_ )         delete project_manager_;
    if ( painter_controller_ )      delete painter_controller_;
    if ( alg_dialog_controller_ )   delete alg_dialog_controller_;
    if ( cluster_tree_widget_ )     delete cluster_tree_widget_;
}

uint MainWindow::readConfig() {
    QDomDocument doc;
    QFile config_file(QApplication::applicationDirPath() + "\\config\\config.xml");

    if ( error_handle( ( !config_file.open( QIODevice::ReadOnly )), err_text("ERR_CONF_FILE_OPEN") + config_file.fileName() ) ) return 1;

    parseXML(doc, config_file);

    QDomNodeList node_list = doc.elementsByTagName("project");
    QDomElement element;
    int count = node_list.count();
    for ( int i = 0; i < count; i++ ) {
        element = node_list.at( i ).toElement();
        if ( error_handle( ( ! element.hasAttribute("path")), ( err_text(tr("ERR_CONF_FILE_READ")) + tr("config.xml, �� ������ ���� � �������.") ) ) ) continue;
        project_manager_->readProject( element.attribute(QString("path")) );
    }
    config_file.close();

//    cluster_tree_widget_ = new ClusterTreeWidget( this );
}

void MainWindow::createToolBars() {
//    static QToolBar* selection_tool_bar = new QToolBar( tr("��������"), this->ui->mainToolBar ) ;
//    selection_tool_bar->addAction( selection_action );
}
