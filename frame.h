#ifndef FRAME_H
#define FRAME_H

#include <QWidget>
#include <set>


typedef std::list<QPoint> QPointList;

QRect       convertQPointListToQRect(QPointList& qpoint_list);
QPointList  convertQRectToQPointList(QRect& qrect);

class Selection {
public:
    enum Type {
        SelectionType,
        FrameSelectionType,
        ArbitrarySelectionType,
        UserSpecifiedType
    };
protected:
    Selection::Type type_;
public:
    virtual const Type type() { return type_; }

protected:
    QWidget* view_;

public:
    Selection() : view_( 0 ), type_( SelectionType ) {  }
    virtual ~Selection() { if ( view_ ) delete view_; }
    QWidget* getView () { return view_;  }
    void setView( QWidget* const  view ) { view_ = view; }

    virtual void move( int dx, int dy ) = 0;
    virtual void moveTo( int x, int y ) = 0;
};

class FrameSelection : public Selection
{
private:
    QRect area_;
    bool modifiable_;

public:
    FrameSelection( const QRect& area, QWidget* view = 0, bool modifiable = true );
    ~FrameSelection() {}
//    Frame( const QPointList& area, QWidget* view = 0, bool sorted = false, bool modifiable = false );
    void    resize( const QRect new_size );
    void    resize( int w, int h );
    void    move( int dx, int dy ); // x_ = x_+dx,   y_ = y_+dy
    void    moveTo( int x, int y ); // x_ = x,       y_ = y

    operator QRect() { return area_; }

    QRect & getArea() { return area_; }
    void    setArea( const QRect& area ) { area_ = area; }
    const Type type() { return type_; }
};

//typedef std::set<FrameSelection*> SelectionptrSet;

typedef std::set<Selection*> SelectionPtrSet;
#endif // FRAME_H
