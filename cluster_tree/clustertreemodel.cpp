#include <QPixmap>
#include "clustertreemodel.h"
#include "core.h"


ClusterTreeModel::ClusterTreeModel(QObject *parent) :
    QAbstractItemModel(parent)
{
    root_ = new Cluster(0,0,0,-1);
}

ClusterTreeModel::~ClusterTreeModel() {
    if ( root_ ) delete root_;
}

Cluster* ClusterTreeModel::clusterFromIndex( const QModelIndex &index ) const {
    if ( index.isValid() )
        return static_cast<Cluster*>( index.internalPointer() );
    else {
        return root_;
    }
}

QModelIndex ClusterTreeModel::index( int row, int column, const QModelIndex& parent ) const {
    if ( !root_ || row <0 || column < 0 ) return QModelIndex();

    Cluster* parent_cluster = clusterFromIndex( parent );
    if ( !parent_cluster ) return QModelIndex();
    if ( parent_cluster->children()->size() >= row ) return QModelIndex();
    Cluster* child_cluster  = parent_cluster->childAt( row );

    return createIndex( row, column, child_cluster );
}

QModelIndex ClusterTreeModel::parent( const QModelIndex& child ) const {
    Cluster* cluster = clusterFromIndex( child );
    if ( cluster ) return QModelIndex();

    Cluster* parent = cluster->getParent();
    if ( !parent ) return QModelIndex();

    Cluster* grand_parent = parent->getParent();
    if ( !grand_parent ) return QModelIndex();

    int children_vec_size = grand_parent->children()->size();
    for( int row = 0; row < children_vec_size; row++)
        if ( grand_parent->childAt( row ) == parent )
            return createIndex( row, 0, parent );

    return QModelIndex();
}

QVariant ClusterTreeModel::data( const QModelIndex &index, int role ) const {
    if ( !index.isValid() ) return QVariant();

    if ( index.column() == 0 && role == Qt::DecorationRole ) { // create pixmap and fill color
        QColor color( static_cast<Cluster*>( index.internalPointer() )->getColor() );
        QPixmap pixmap( 20, 20 );
        pixmap.fill( color );
        return pixmap;
    }
    if ( index.column() == 1 && role == Qt::DisplayRole ) {
        Cluster* cluster = static_cast<Cluster*>( index.internalPointer() );

        if ( index.row() > cluster->children()->size() ) {

        }
        QString stat_represent = cvMatQStringRepresent( cluster->getStatistics() );
        return stat_represent;
    }
}

QVariant ClusterTreeModel::headerData( int section, Qt::Orientation orientation, int role ) const {
    if ( orientation == Qt::Horizontal && role == Qt::DisplayRole ) {
        if ( section == 0 )
            return tr( "Цвет" );
        else if ( section == 1 )
            return tr( "Данные" );
        return QVariant();
    }
}

int ClusterTreeModel::rowCount( const QModelIndex &parent ) const {
    if ( parent.column() > 0 )  return 0;

    Cluster* parent_cluster;
    if ( ! parent.isValid() ) parent_cluster = root_;

    return parent_cluster->children()->size();
}

int ClusterTreeModel::columnCount( const QModelIndex &parent ) const {
    Cluster* parent_cluster;
    if ( ! parent.isValid() ) parent_cluster = root_;

    return 2 + parent_cluster->data()->size();
}

//void ClusterTreeModel::appendToRoot( Cluster *cluster ) {
//    root_->appendChild( cluster );
//}

//void ClusterTreeModel::removeFromRoot( int index ) {
//    root_->removeChild( index );
//}
