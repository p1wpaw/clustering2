#ifndef CLUSTERTREEMODEL_H
#define CLUSTERTREEMODEL_H

#include <QAbstractItemModel>
#include "cluster.h"

class ClusterTreeModel : public QAbstractItemModel
{
    Q_OBJECT
private:
    Cluster* root_;

public:
    void appendToRoot( Cluster* cluster ) { root_->children()->push_back( cluster ); }
    void removeFromRoot( int index ) {  root_->removeChild( index ); }

    explicit ClusterTreeModel(QObject *parent = 0);
    ~ClusterTreeModel();

    QModelIndex index( int row, int column, const QModelIndex& parent ) const;
    QModelIndex parent( const QModelIndex& child ) const;

    int rowCount    ( const QModelIndex &parent ) const;
    int columnCount ( const QModelIndex &parent ) const;

    QVariant data( const QModelIndex &index, int role ) const;
    QVariant headerData( int section, Qt::Orientation orientation, int role ) const;

    Cluster* clusterFromIndex( const QModelIndex& index ) const ;

    Cluster* const getRoot() { return root_; }

signals:
    
public slots:
    
};

#endif // CLUSTERTREEMODEL_H
