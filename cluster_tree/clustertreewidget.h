#ifndef CLUSTERTREEWIDGET_H
#define CLUSTERTREEWIDGET_H

#include <QWidget>
#include "algorithms/algorithmdialogcontroller.h"
#include "painter_contexts/paintercontroller.h"

namespace Ui {
    class ClusterTreeWidget;
    }

class ClusterTreeWidget : public QWidget
{
    Q_OBJECT
    
public:
    explicit ClusterTreeWidget( QWidget*                    parent = 0,
                                PainterController*          painter_controller_ = 0,
                                AlgorithmDialogController*  alg_dialog_controller = 0 );
    ~ClusterTreeWidget();
    
private:
    Ui::ClusterTreeWidget *ui;
    Project* project_;
    ClusterTreeModel* cluster_tree_model_;
    PainterController*          painter_controller_;
    AlgorithmDialogController*  alg_dialog_controller_;

public:
    Ui::ClusterTreeWidget* const getUi() { return ui; }
    void customEvent( QEvent *event );

    void setAlgorithmDialogController ( AlgorithmDialogController* alg_dialog_controller )
        { alg_dialog_controller_ = alg_dialog_controller; }
    AlgorithmDialogController* getAlgorithmDialogController()
        { return alg_dialog_controller_; }

    void setPainterController ( PainterController* painter_controller )
        { painter_controller_ = painter_controller;   }
    PainterController* getPainterController()
        { return painter_controller_; }

    void        setProject( Project* project );
    Project*    getProject()                   { return     project_; }
};

#endif // CLUSTERTREEWIDGET_H
