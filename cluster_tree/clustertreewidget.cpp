#include "clustertreewidget.h"
#include "ui_clustertreewidget.h"


ClusterTreeWidget::ClusterTreeWidget( QWidget *parent,
                                      PainterController* painter_controller,
                                      AlgorithmDialogController* alg_dialog_controller) :
    QWidget( 0 ),
    painter_controller_( painter_controller ),
    alg_dialog_controller_( alg_dialog_controller ),
    ui(new Ui::ClusterTreeWidget)
{
    ui->setupUi(this);
    if ( alg_dialog_controller_ )
        alg_dialog_controller_->setClusterTreeWidget( this );
//    alg_dialog_controller->setClusterTreeWidget( this );
}

ClusterTreeWidget::~ClusterTreeWidget()
{
    delete ui;
}

void ClusterTreeWidget::setProject( Project *project ){
    project_ = project;
    cluster_tree_model_ = project_->modelStorage()->clusterTree();
    project_->modelStorage()->algorithmIOController()->setClusterTreeWidget( this );
}

void ClusterTreeWidget::customEvent( QEvent *e ) {
    qDebug() << "cluster_tree_wdgt::event" << alg_dialog_controller_ << painter_controller_;
    if ( e->type() == static_cast<QEvent::Type>( QEvent::User + 2 ) ) {
        if ( !painter_controller_ || !alg_dialog_controller_ ) return;
        AllAlgorithmsCompleteEvent*
            aall_event = static_cast<AllAlgorithmsCompleteEvent*>( e );

        ClusterPainterContext*
            cluster_context = static_cast <ClusterPainterContext*>  (
                                            painter_controller_->
                                            getContexts().
                                            find( PainterContext::ClusterLayer )->
                                            second );

        QString key = aall_event->first()->key();

        AlgorithmIOController* alg_io_controller =
                aall_event->getAlgorithmIOContoller();

        AlgorithmIOController::IOMap::iterator it
                = alg_io_controller->ioMap()->find( key );


        Cluster* parent_cluster = aall_event->first()->cluster();
        cluster_tree_model_->getRoot()->appendChild( parent_cluster );
        painter_controller_->setClusterContext( parent_cluster );
        show();
    }
    return QWidget::customEvent( e );
}

//void ClusterTreeWidget::setRoot( Cluster *root ) {
//    root_ = root;
//    if ( !root_ ) return;
//}
