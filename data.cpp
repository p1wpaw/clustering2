#include "data.h"
#include "core.h"


//______________________________________________________________________________________________________________
// Data implementations
Data::Data( cv::Mat* const  image,
            cv::Mat* const  labels,
            const uint      dimention,
            const uint      i,
            const uint      j,
            const QPoint&   offset
           )
    : dimention_(dimention), image_(image)
{
    offset_operator_.setLabels( labels );
    offset_operator_.setOffset( offset );
}
Data::~Data() {

}

double      Data::operator []( uint index ) { return 0; }

//double&   Data::getCentroidComponent( uint index ) {}

//void      Data::setCoordinates( uint i, uint j ) {
//    statisitcs_.at<uint>( 0, 0 ) = i;
//    statisitcs_.at<uint>( 0, 1 ) = j;
//}

//void      Data::setCoordinates( const DataIndex& index ) {
//    statisitcs_.at<uint>( 0, 0 ) = index.i;
//    statisitcs_.at<uint>( 0, 1 ) = index.j;
//}


cv::Mat* Data::getLabels(){
    return offset_operator_.getLabels();
}

void Data::setLabels( cv::Mat* labels ) {
    offset_operator_.setLabels( labels );
}

//______________________________________________________________________________________________________________
// StoredData implementations
StoredData::StoredData( cv::Mat* const  image,
                        cv::Mat* const  labels,
                        const uint      dimention,
                        const uint      i,
                        const uint      j )
    : Data( image, labels, dimention )  {
    if ( dimention_ < 5 ) { return; }
    statisitcs_.create( 1, dimention_, cv::DataType<int>::type );
    setCoordinates( i, j );

    label_ = offset_operator_.getLabels()->at<uint>( i, j );
}
StoredData::StoredData(  ) {  }

double      StoredData::operator []( uint index ) {
    return (double) statisitcs_.at<int>( 0, index );
}

double      StoredData::getCentroidComponent( uint index ) {
    return (double) statisitcs_.at<int>( 0, index );
}

void        StoredData::setCoordinates( int i, int j ) {
    QRgb color = image_->at<uint>( i, j );

    CvMatIndex index( &statisitcs_ );
    index.at<int>( 0, 0 ) = i;
    index.at<int>( 0, 1 ) = j;
    index.at<int>( 0, 2 ) = rgbVecComponent( color, 0 );
    index.at<int>( 0, 3 ) = rgbVecComponent( color, 1 );
    index.at<int>( 0, 4 ) = rgbVecComponent( color, 2 );

    qDebug()  << statisitcs_.at<int>( 0, 0 )
              << statisitcs_.at<int>( 0, 1 )
              << statisitcs_.at<int>( 0, 2 )
              << statisitcs_.at<int>( 0, 3 )
              << statisitcs_.at<int>( 0, 4 );
}

Data::DataIndex StoredData::getCoordinates() {
    return (DataIndex {
        statisitcs_.at<int>( 0, 0 ),
        statisitcs_.at<int>( 0, 1 )
    });
}

QRgb StoredData::getColor() {
    QRgb    color = image_->at<uint>(   statisitcs_.at<int>( 0, 0 ),
                                        statisitcs_.at<int>( 0, 1 ) );
    return  color;
}

void StoredData::setLabel( int label ) {
    Data::setLabel( label );
    if ( ! getLabels() ) return;

    CvMatIndex index( getLabels() );

    int i = statisitcs_.at<int>( 0, 0 ),
        j = statisitcs_.at<int>( 0, 1 );

    index.at<int>( i, j ) = label_;
}

//______________________________________________________________________________________________________________
// CvImageIndex implementations

//CvImageIndex::CvImageIndex( cv::Mat * image, uint stat_vector_dim ) :
//    image_(image),
//    stat_vector_dim_(stat_vector_dim) {

//}
//CvImageIndex::~CvImageIndex() {}

//#ifdef unix
//template <> Data CvImageIndex::at<Data>( uint i, uint j, uint k=0 ) {
//    Data data( image_, 3, i, j );
//    return data;
//}
//#endif
